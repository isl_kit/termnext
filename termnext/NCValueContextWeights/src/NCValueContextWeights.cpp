/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include "lapos-0.1.2/postagger.h"

vector<vector<TuplePair> >units;
postagger* postagger_;

class Context{
public:
	Context(){count=0; weight=0;};
	~Context(){};
	Context(string s, int c) {value=s; count=c; weight=0;}
	string value;
	int count;
	double weight;
};

string tags[] = {"JJ", "JJR", "JJS", "ADJ", "ADV", "FW", "N", "NP", "NN", "NNS", "NNPS", "NUM", "V", "VD", "VG", "VN", "VBN", "VB", "VBD", "VBG", "VBZ"};
std::set<std::string> POSTAGs(tags, tags+21);

map<string, Context> contextWordsBefore;
map<string, Context> contextWordsAfter;

void addContextWordBefore(string word){
	if (contextWordsBefore.find(word) == contextWordsBefore.end()){
		Context t(word,1);
		contextWordsBefore.insert(make_pair(word,t));
	}
	else{
		contextWordsBefore[word].count++;
	}
}

void addContextWordAfter(string word){
	if (contextWordsAfter.find(word) == contextWordsAfter.end()){
		Context t(word,1);
		contextWordsAfter.insert(make_pair(word,t));
	}
	else{
		contextWordsAfter[word].count++;
	}
}

bool posAccepted(string postag){
	if (POSTAGs.find(postag)!= POSTAGs.end()) return 1;
	else return 0;
}

void calculateWeights(int totalNumberOfTerms){
	std::map<string, Context>::iterator it;
	for (it=contextWordsBefore.begin(); it!=contextWordsBefore.end(); it++){
	  it->second.weight = (double)it->second.count/(double)totalNumberOfTerms;
	}
	
	for (it=contextWordsAfter.begin(); it!=contextWordsAfter.end(); it++){
	  it->second.weight = (double)it->second.count/(double)totalNumberOfTerms;
	}
}

bool comp (Context a, Context b){return (a.weight < b.weight);}

void print(string nameBase){
  string outBefore = nameBase+".before"; 
  ofstream fileoutB; fileoutB.open(outBefore.c_str());
  string outAfter = nameBase+".after";   
  ofstream fileoutA; fileoutA.open(outAfter.c_str());
  
	vector<Context> vec;
	std::map<string, Context>::iterator it;
	for (it=contextWordsBefore.begin(); it!=contextWordsBefore.end(); it++){
		vec.push_back(it->second);
	}
	sort(vec.begin(), vec.end(), comp);
	for (int i=0; i<vec.size(); i++)
		fileoutB << vec[i].value << "\t#\t"<< vec[i].weight <<"\t#\t"<< vec[i].count << endl;
	
	vec.clear();

	for (it=contextWordsAfter.begin(); it!=contextWordsAfter.end(); it++){
		vec.push_back(it->second);
	}
	sort(vec.begin(), vec.end(), comp);
	for (int i=0; i<vec.size(); i++)
		fileoutA << vec[i].value << "\t#\t"<< vec[i].weight <<"\t#\t"<< vec[i].count << endl;	
	
	
	fileoutB.close(); fileoutA.close();
}

void postagging(stringstream& datastream){
	cout << "...pos tagging..." << endl;

	string document = datastream.str();
	istringstream in(document);
	string line;
	while (getline(in, line)){

		int length = line.length();

		if (length < 500) {
			vector<TuplePair> tags = postagger_->tag(line);
			units.push_back(tags);

		}
		else {

			int pos=0;
			while (pos<length){
				string segment = line.substr(pos,500);
				vector<TuplePair> tags = postagger_->tag(segment);
				units.push_back(tags);
				pos=pos+500;
			}
		}
	}
}

std::string int2str(int i)
{
	std::string s;
	std::stringstream out;
	out << i;
	s = out.str();

	return s;
}


int str2int(std::string s)
{
	std::istringstream item(s);
	int value;
	item >> value;

	return value;
}


using namespace std;

int main(int argc, char* argv[]) {
string textName="";
int window=10;
int posuse=0;


int totalNumberOfTerms=0;

for (int i=1; i<argc; ++i)
{
	if (argc==1)  { cerr << argv[0] << " -help" << endl; return 0;}
	string s = string(argv[i]);
	if (s == "-txt") { textName= string(argv[++i]);}
	else if (s == "-win") { window = atoi(argv[++i]); }
	else if (s == "-pos") {posuse=atoi(argv[++i]); }
	else if((s=="-h") || (s=="-help")) {
	     cerr << "USAGE: ";
	     cerr << argv[0] << " -txt [taged file] -win [default +-1] -pos [0|1]\n";
	     cerr << "Learn weights of context words for NC-Value\n";
	     cerr << "Output: word # weight # count \n";
	     return 0;
	}
	else { cerr << argv[0] << "-help" << endl; return 0;}
}
if (textName=="") {cerr << argv[0] << " -help" << endl; return 0;}

string outFileName=textName+".Win"+int2str(window);
postagger ptagger;
ptagger.loadModel("/home/yzhang/Tools/lapos-0.1.2/model_wsj00-18/");

ifstream ifstream_;
ifstream_.open(textName.c_str());
stringstream datastream;
datastream << ifstream_.rdbuf();
postagging(datastream);

bool isTerm=false;
for (int k=0; k<units.size(); ++k){
for (int i=0; i<units[k].size(); ++i) {
	if (units[k][i].key == "$term{"){
		isTerm = true;
		totalNumberOfTerms++;
		for (int j=i-1; j>=max(0,i-window); j--){
			string w=units[k][j].key;
			string pos=units[k][j].value;
			if(posuse==1){
			  if(posAccepted(pos)){
				addContextWordBefore(w);
			  }
			}
			else addContextWordBefore(w);
		}
	}
	else if ((units[k][i].key == "}")&&(isTerm==true)) {
		for (int j=i+1; j<=min((int)units[k].size()-1,i+window); j++){
			string w=units[k][j].key;
			string pos=units[k][j].value;
			if(posuse==1){
			  if(posAccepted(pos)){
				addContextWordAfter(w);
			  }
			}
			else addContextWordAfter(w);
		}
		isTerm = false;
	}
}
}
	calculateWeights(totalNumberOfTerms);
	print(outFileName);
	cerr << "Extraction Done" << endl;
}
