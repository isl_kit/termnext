/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CNCVALUE_H_
#define CNCVALUE_H_

#include <string.h>
#include <stdio.h>
#include <fstream>
#include <sstream>

using namespace std;

struct CValueTrip{
        CValueTrip(){f=0; t=0; c=0;};
        CValueTrip(double newf, double newt, double newc){f=newf; t=newt; c=newc;};

        double f; //frequency of term a in the whole corpus
        double t; //frequency of term a as a nested string of longer candidate terms
        double c; //number of these longer candidate terms

        void set(double newf, double newt, double newc) {f=newf; t=newt; c=newc;};
};

#endif 
