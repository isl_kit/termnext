/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ConfigLoader.h"

Config* cfg;

ConfigLoader::ConfigLoader() {
}

ConfigLoader::~ConfigLoader() {
}

int ConfigLoader::init(const char* confFilePath){
 	cout << "Reading config file ..." << endl;

	cfg = new libconfig::Config();
	try{
	   cfg->readFile(confFilePath);
	}
	catch(FileIOException & fioex)
	{
		cerr << "Can not open file Preferences.cfg";
		return 0;
	}
	catch(ParseException & ex)
	{
		cerr << std::string("Can not correctly parse Preferences.cfg: ") + ex.what();
		return 0;
	}

	return 1;
}

vector<Type> ConfigLoader::loadNETypes(){
		vector<Type> allTypes;
		const Setting& nelistsettings = cfg->lookup("namedentities.list");

		int id;
		string name;
		string label;
		string color;
		for(int i=0; i<nelistsettings.getLength(); i++){
			Type t;
			const Setting& ne = nelistsettings[i];
			ne.lookupValue("id", id);
			ne.lookupValue("name", name);
			ne.lookupValue("label", label);
			ne.lookupValue("color", color);
			t.id = id;
			t.name = name;
			t.label = label;
			t.color = color;
			allTypes.push_back(t);
		}
		return allTypes;
}

vector<string> ConfigLoader::loadTermList(){
	vector<string> termList;
	const Setting& tlistsettings = cfg->lookup("te-supported-pos-taggs.list");
	for(int i=0; i<tlistsettings.getLength(); i++){
		const Setting& setting = tlistsettings[i];
		termList.push_back(string(setting.c_str()));
	}
	return termList;
}

int ConfigLoader::loadMaxlengthTerm(){
	int MaxLen = cfg->lookup("maxlength-terminology");
	return MaxLen;
}
