/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CONFIGLOADER_H_
#define CONFIGLOADER_H_

#include <iostream>
#include <libconfig.h++>
#include <string.h>
#include <vector>
#include "named_entities/Types.h"

using namespace std;
using namespace libconfig;

struct POS{
	string name;
};

class ConfigLoader {
public:
	ConfigLoader();
	virtual ~ConfigLoader();
	int init(const char* confFilePath);
	vector<Type> loadNETypes();
	vector<string> loadTermList();
	int loadMaxlengthTerm();
};

#endif /* CONFIGLOADER_H_ */
