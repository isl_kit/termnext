/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <Python.h>
#include "DocumentFilter.h"
#include <dirent.h>
#include <sys/stat.h>
#include <fstream>
#include "libstemmer_c/include/libstemmer.h"
#include <algorithm>

struct sb_stemmer* word_stemmer__;

double databaseDocTotalCount;
map<string, pair<double,double> > databaseWordsOccurenceMap; // double 1: total freq of string in the database; double 2: doc number of string occurring in database


map<string, TargetWord> wordsOccurrenceMap;
map<int, double> docId2MostFrequentWordNr;

TFIDF* tfidf;
string docDir;
bool scoreDebug=1;
bool BigFreq=0; //0: Term freq is from uploaded files; 1: Term freq is from uploaded files+background docs.
int ScoreOn=31;  // 0001: importanceScore is tfidf; 0010: cvalue; 0100: nvalue  1000: wiki;  10000: feature score
				//eg: ScoreOn=1  importantscore = tfidf
				//: ScoreOn=3  importantscore = tfidf + cvalue
				//: ScoreOn=7  importantscore = tfidf + cvalue + ncvalue
				//: ScoreOn=15 importantscore = tfidf + cvalue + ncvalue + wiki
				//: ScoreOn=9  importantscore = tfidf + wiki
				//: ScoreOn=31 importantscore = tfidf + cvalue + ncvalue + + wiki +feature score
vector<string> neStopWordsList;
vector<string> wikiList;

/*PyObject* tockenized_borrowed_ref;
PyObject* pos_borrowed_ref;*/

struct Word {
	string value;

	bool isNumber(){
		if(value.length() != 0){
			return isdigit(value[0]);
		}
		return false;
	}
};

bool myfunction(Term a, Term b){return (a.length()>b.length());}

DocumentFilter::DocumentFilter(vector<string> suppotedPOSTags, const char* docDr, const char* modelpath, vector<char*> allCountries, bool excludeStopWords, char** neStopWords, int size, Gramm ngramm) {
	this->allCountries = allCountries;
	this->suppotedPOSTags = suppotedPOSTags;

	word_stemmer__ = sb_stemmer_new("english", NULL);

    // create a folder with name generate to store generated files
	docDir = string(docDr);
    string genDir = docDir+"/generated/";
    mkdir(genDir.c_str(), 0777);

    for(int i=0; i<size; i++){
    	loadNEStopWords((char*)neStopWords[i]);
    }

	pair<FilePathNode*, int> flPathNodePair =  readAllFileNames(string(docDir));
	FilePathNode* filePathNode = flPathNodePair.first;
	int totalNrOfDocs = flPathNodePair.second;

	tfidf = new TFIDF(excludeStopWords);

	FilePathNode* filePathNodeTmp = filePathNode;
    int docCounter = 1;
    while(filePathNodeTmp != 0){
		string filePath = filePathNodeTmp->data;
		tfidf->calculateWordOccurrenceByFile(filePath.c_str(), docCounter++, wordsOccurrenceMap, docId2MostFrequentWordNr, ngramm);
		filePathNodeTmp = filePathNodeTmp->next;
	}
}

DocumentFilter::DocumentFilter() {
}

DocumentFilter::~DocumentFilter() {

}

void DocumentFilter::init(vector<string> suppotedPOSTags, vector<char*> allCountries, bool excludeStopWords){
	this->allCountries = allCountries;
	this->suppotedPOSTags = suppotedPOSTags;

	word_stemmer__ = sb_stemmer_new("english", NULL);
}

void DocumentFilter::reset(){
	wordsOccurrenceMap.clear();
	docId2MostFrequentWordNr.clear();
}

void DocumentFilter::calculateWordOccurrence(stringstream& datastream, int docCounter, Gramm ngramm){
	cout << "...calculating word occurrence..." << endl;
	tfidf->calculateWordOccurrence(datastream, docCounter++, wordsOccurrenceMap, docId2MostFrequentWordNr, ngramm);
	cout << "WordsOccurrenceMap.size() "<< wordsOccurrenceMap.size()<< endl;
}


void DocumentFilter::postagging(stringstream& datastream, string language){
	cout << "...pos tagging...on " << language << endl;

	units.clear();

	string document = datastream.str();
	istringstream in(document);
	string line;
	while (getline(in, line)){
		int length = line.length();

		if((language == "en")||(language=="EN")){

			if (length < 200) {
				vector<TuplePair> tags = postagger_->tag(line);
				units.push_back(tags);
			}
			else {
				int pos=0;
				while (pos<length){
					string segment = line.substr(pos,200);
					vector<TuplePair> tags = postagger_->tag(segment);
					units.push_back(tags);
					pos=pos+200;
				}
			}
		}
/*		else if ((language == "de")||(language=="DE")){

			vector<TuplePair> Tags;// for one sentence

			// Run German POS tagging on each input sentence; save the result in "outname"
			stringstream command;
			command << " echo \""<< line << "\" | /bin/bash /home/yzhang/src/eu-bridge/EU-Bridge/src/posTagging/GermanPOSTagging.sh" << "";
	//cerr << "AA: " << command.str() << endl;
			// Pipe command to a temporary file
			string outtags = exec(command.str().c_str());

	//	cerr << "AA " << outtags << endl;
			istringstream in(outtags);
			string oneline, dump;
			TuplePair pair;
			while (getline(in, oneline)){
				istringstream item(oneline);
				item >> pair.key;
				item >> pair.value;
				item >> dump;
				Tags.push_back(pair);
			}

			units.push_back(Tags);
		}
*/
		else{ // for other languages without POS tagging
			vector<TuplePair> tags; string word;
			istringstream item(line);
			while(item>>word){
				TuplePair tmp; tmp.key = word; tmp.value="XX"; //
				tags.push_back(tmp);
			}
			units.push_back(tags);
		}
	}

}

int DocumentFilter::loadWiKi(const string filename, const string language){
	cerr << "load " << language << " wiki terms: " << endl;

	ifstream ifstream_;
	ifstream_.open(filename.c_str());
	string line;
	while (std::getline(ifstream_, line)){
		istringstream item(line);
		int count; item >> count;
		string wikiString ="", w;
		while (item >> w){
			wikiString+=w+" ";}

		TFIDF dump; dump.trimWord(wikiString);
		std::transform(wikiString.begin(), wikiString.end(), wikiString.begin(), ::tolower);
		wikiList.insert(wikiString);
	}

	if (wikiList.size()==0) {cerr << " Loading wiki list Failed!"<< endl; return 0;}
	else {cerr << "wiki List size " << wikiList.size() << endl;}

//for (set<string>::iterator it = wikiList.begin(); it!=wikiList.end(); ++it)
//	cerr << *it << endl;
	return wikiList.size();
}

int DocumentFilter::loadPosPatterns(const string filename, string language){

cerr << "POS Pattern:" <<filename << " for " << language << endl;

	ifstream ifstream_;

	// get total count
	double TotalCount=0;
	ifstream_.open(filename.c_str());
	string line;
	cerr << "...get total count of POS patterns...:";
	while (std::getline(ifstream_, line)){
		istringstream item(line);
		int count; item >> count;
		TotalCount = TotalCount+count;
	}
	cerr << TotalCount << endl;
	ifstream_.close();

	ifstream_.open(filename.c_str());
	cerr << "...Load POS patterns...:";
	while (std::getline(ifstream_, line)){
		istringstream item(line);
		int count; item >> count;
		string pattern="", tag;
		while (item >> tag){
			pattern+=tag+" ";}
		supportedPosPatterns[language].insert(pair<string, double>(pattern, (double)count/TotalCount));
	}
	ifstream_.close();
	if (supportedPosPatterns[language].size()==0) {cerr << " Failed!"<< endl; return 0;}
	else {cerr << "supportedPosPatterns for " << language << " " << supportedPosPatterns[language].size() << endl;
	}
	return supportedPosPatterns[language].size();
}

int DocumentFilter::loadTermBlackList(const string filename){

	cerr << "Black List:" <<filename << endl;

	ifstream ifstream_;
	ifstream_.open(filename.c_str());
	string line;
	cerr << "...Load Term Black Words...:";
	while (std::getline(ifstream_, line)){
		TFIDF dump;
		dump.trimWord(line);
		if(line!="") termBlackList.insert(line);
	}
	if (termBlackList.size()==0)  {cerr << " Failed!"<< endl; return 0;}
	else cerr << termBlackList.size() << endl;
	return termBlackList.size();
}

void DocumentFilter::loadNEStopWords(const string filename){
	ifstream in;
	in.open(filename.c_str());
	string line;
	while(getline(in, line) != NULL){
		neStopWordsList.push_back(line);
	}
	in.close();
}


map<string, TargetWord>::key_type get_key(map<string, TargetWord>::value_type aPair) {
  return aPair.first;
}

string DocumentFilter::getNGramm(Gramm ngramm, map<string, TargetWord>::iterator& i, map<string, TargetWord>::iterator keyend){
	string uni_ = i->first;
	if(ngramm == Uni){
		return uni_;
	}

	if(ngramm == Bi || ngramm == Tri){
	//	map<string, TargetWord>::iterator next_i;
	//	next_i = i;
	//	 next_i++;
		 if(++i != keyend){
			 if (ngramm == Bi){
				 string bi_ = (string)i->first;
				 cout << uni_ + " " << bi_ << endl;
				 i--;
				 return uni_ + " " + bi_;
			 }else{ // ngramm == Tri
//				 boost::transform_iterator<get_key_t, map_iterator> next_next_i = next_i;
//				 next_next_i++;
//				 if(next_next_i != keyend){
//				 		string tri_ = *next_next_i;
//				 		return uni_ + " " + bi_ + " " + tri_;
//				 }
			 }
		 }
	}
	return NULL;
}

const char* DocumentFilter::getWordStamm(string word){
	  string word_copy = to_lower_copy(word);
	  sb_symbol* word_copy_symb = (sb_symbol*)malloc(word_copy.size() * sizeof(sb_symbol));
	  for(unsigned int j=0; j<word_copy.size(); j++){
			  word_copy_symb[j] = (int)word_copy[j];
	  }
	  const sb_symbol* word_copy_symbol = sb_stemmer_stem(word_stemmer__, word_copy_symb, word.size());
	  free(word_copy_symb);
	  char* word_copy_symbol_buffer = (char*)malloc(word.size() * sizeof(char));
	  const char* word_copy_symbo_l = (const char*)word_copy_symbol;
	  word_copy_symbol_buffer = strcpy(word_copy_symbol_buffer, word_copy_symbo_l);
	  string newstr(word_copy_symbol_buffer);
	  //free(word_copy_symbol_buffer);
	  return newstr.c_str();
}

vector<Term> DocumentFilter::filterByPOSPatterns(const int MaxTermLen, const string language) {
	vector<Term> termCandidates;
	// filter by pos patterns
	// filter by blackword list
	// filter by illegal term boundaries

	int totalWordNumbers=0;
	TFIDF dump;
	int t=0;

	if(units.size()==0) {cerr << "Warning: units.size = 0" << endl; return vector<Term>(0);}

	for (int k=0; k<units.size(); ++k){
		int sentLength = units[k].size();
		totalWordNumbers = totalWordNumbers+sentLength;
	for (int i=0; i<sentLength;++i) {
		string words = ""; string tags="";
		vector<TuplePair> beforeVec, afterVec;
		TuplePair tptmp;

		// get [-2,0) contexts
		if(i-1>=0){tptmp.key=units[k][i-1].key; tptmp.value=units[k][i-1].value; beforeVec.push_back(tptmp);}
		if(i-2>=0){tptmp.key=units[k][i-2].key; tptmp.value=units[k][i-2].value; beforeVec.push_back(tptmp);}

		for (int j=i; j<std::min(i+MaxTermLen,sentLength);++j){
			string key = units[k][j].key;

			//dump.trimWord(key);
			words = words + " " + key;
			tags  = tags + units[k][j].value + " ";

			// get [-2,0) contexts
			if(j+1<sentLength){tptmp.key=units[k][j+1].key; tptmp.value=units[k][j+1].value; afterVec.push_back(tptmp);}
			if(j+2<sentLength){tptmp.key=units[k][j+2].key; tptmp.value=units[k][j+2].value; afterVec.push_back(tptmp);}



			std::map<string, double>::iterator it;
			it = supportedPosPatterns[language].find(tags);

			if ((supportedPosPatterns[language].size()==0)||(it != supportedPosPatterns[language].end())){
				// if the term's POS string is supported (English) or the POS pattern list is empty(for other language)
				// I don't know why supportedPosPatterns.size()==1 for German empty POS pattern list!!!???
				Term t;
				string term=words;

				dump.trimWord(term);
				t.value = term;

				if(supportedPosPatterns[language].size()>0){
					t.countOfPattern = supportedPosPatterns[language][tags];}
				else t.countOfPattern=0;

				t.setpos(tags);
				t.afterContext=afterVec; t.beforeContext=beforeVec;
				afterVec.clear(); beforeVec.clear();

				vector<Term>::iterator tmpit=std::find(termCandidates.begin(), termCandidates.end(), t);
				if ( tmpit== termCandidates.end()){ // t is a new term
					if (isValid(t, language)) {termCandidates.push_back(t); }
				}
				else {
					(*tmpit).afterContext.insert((*tmpit).afterContext.end(), t.afterContext.begin(), t.afterContext.end());
					(*tmpit).beforeContext.insert((*tmpit).beforeContext.end(), t.beforeContext.begin(), t.beforeContext.end());
				}
			}
	//		}
		}
	}
	}


	if (scoreDebug==1){
	 string filePathTFIDFilteredWS = "../all.tfidf.filtered.word.score"; // output file
	 ofstream outwstfidf;
	 outwstfidf.open(filePathTFIDFilteredWS.c_str());
	 outwstfidf << "Total Number of words in doc: " << totalWordNumbers << endl;
	}

/*// for Debugging...
  vector<Term> termLists = removeShorterOneInSimilarCandidates(termCandidates);

	if(termLists.size()==0) return vector<Term>(NULL);
	cerr << "After Filtering " << 	termCandidates.size() << endl;
	cerr << "After removing similar candidates: " << termLists.size() << endl;
	return vector<Term>(&termLists[0], &termLists[termLists.size()]);
*/
	return vector<Term>(&termCandidates[0], &termCandidates[termCandidates.size()]);
}



void DocumentFilter::filterByTFIDF(vector<Term>& termCandidates, const int totalNrOfDocs){
	 string filePathTFIDFilteredWS = "../all.tfidf.filtered.word.score"; // output file
	 ofstream outwstfidf;
	 outwstfidf.open(filePathTFIDFilteredWS.c_str());

	 for (int i=0; i<termCandidates.size(); ++i) {
		 string term = termCandidates[i].value;
		 map<int,list<pair<int,int>>> docs = wordsOccurrenceMap[term].docs;

		 vector<double> tfScores;
		 for(map<int,list<pair<int,int>>>::iterator docsiter = docs.begin(); docsiter != docs.end(); docsiter++){
			 double mostFrequentWord = docId2MostFrequentWordNr[docsiter->first];
		 	 double tfScore = tfidf->tf(term, docsiter->second.size(), mostFrequentWord);
	 		 tfScores.push_back(tfScore);
		 }

		 //calculate idf score
		 double idfScore = tfidf->idf(term, docs.size(), totalNrOfDocs);
		 //double word_occurrance = (double)wordsOccurrenceMap[term].getTotalNrOfOccurrences();

		 double tfidfScore = tfidf->tfidf(term, tfScores, idfScore);

		 termCandidates[i].tfidf = tfidfScore;

	 }
	 outwstfidf.close();
  }


pair< map<int, Term>, vector<TID2Importance> > DocumentFilter::filterByTFIDFCombiningSingleWords(int termSpecificity, int totalNrOfDocs){
//	  map<string, int> wordsWithoutRedundancy;
	  string filePathTFIDFilteredWS = "../all.tfidf.filtered.word.score"; // output file
//	  string filePathIDFFilteredWS = docDir + "/generated/all.idf.filtered.word.score";
//	  string filePathTerminologyList = docDir + "/generated/terminology.list";
	  ofstream outwstfidf;
//	  ofstream outwsidf;
//	  ofstream outtermlist;
	  outwstfidf.open(filePathTFIDFilteredWS.c_str());
//	  outwsidf.open(filePathIDFFilteredWS.c_str());
//	  outtermlist.open(filePathTerminologyList.c_str());

	  //mapkey_iterator keybegin(wordsOccurrenceMap.begin(), get_key);
	  //mapkey_iterator keyend(wordsOccurrenceMap.end(), get_key);

//	  outwsidf << "Word\tDocs\tOccurrences\tidfScore/word-occurrence\tTotal-Nr-Of--Docs\tIDF" << endl;
//	  outtermlist << "Word\tIn-Doc\tTotal-Doc\tTotal-Nr-Of-Occurrences\tIDF\tTFIDF\n";
//

	  cout << "------------- Terminology List -----------" << endl;

	  map<int, Term> termList;
	  vector<TID2Importance> tID2ImportanceList;
	  int termId = 1;
	  for(map<string, TargetWord>::const_iterator wordOccurrMapIter =  wordsOccurrenceMap.begin(); wordOccurrMapIter != wordsOccurrenceMap.end(); wordOccurrMapIter++){
		  	 string word = wordOccurrMapIter->first;
		  	 TargetWord targetWord = wordOccurrMapIter->second;

		  	 Word word_struct;
			 word_struct.value = word;
			  // calculate all TF Scores
			   vector<double> tfScores;
			   // key -> doc-Id, value -> Number-Of-Target-Words
			   map<int,list<pair<int,int>>> docs = wordsOccurrenceMap[word].docs;
			   for(map<int,list<pair<int,int>>>::iterator docsiter = docs.begin(); docsiter != docs.end(); docsiter++){
			 		 double mostFrequentWord = docId2MostFrequentWordNr[docsiter->first];
			 		 double tfScore = tfidf->tf(word, docsiter->second.size(), mostFrequentWord);
		 			 tfScores.push_back(tfScore);
	 		   }

			   //calculate idf score
			   double idfScore = tfidf->idf(word, docs.size(), totalNrOfDocs);
			   double word_occurrance = (double)wordsOccurrenceMap[word].getTotalNrOfOccurrences();

//			   outwsidf << word << "\t" << docs.size() << "\t" << word_occurrance << "\t";
//			   outwsidf << ((double)idfScore)*((double)1/(double)word_occurrance) << "\t";
//			   outwsidf << totalNrOfDocs << "\t" << idfScore << "\n";

			   double tfidfScore = tfidf->tfidf(word, tfScores, idfScore);
//			   if (wordsWithoutRedundancy[word] != 1){
//			   		 wordsWithoutRedundancy[word] = 1;
			   		 outwstfidf << word << "\t" << idfScore << "\t" ;
			   		 for(int i=0; i<tfScores.size(); i++){
			   			 outwstfidf << tfScores[i] << " ";
			   		 }
			   		 outwstfidf << "\t" << tfidfScore << "\n";
//			    }
//			   cout << word << "\t" << targetWord.docs.size() << "\t" << totalNrOfDocs << "\t" << word_occurrance << "\t" << idfScore << "\t" << tfidfScore << "\n";
			    if(word_occurrance > termSpecificity){
			       	 if(word.size() > 1 && !word_struct.isNumber()){
			        	  // filter out countries based on a list of countries extracted by wikipedia
			        	 // if (find(this->allCountries.begin(), this->allngraCountries.end(), word) ==  this->allCountries.end()){
			        		  Term t(termId, word, tfidfScore);
			        		  t.docs = targetWord.docs;
			        		  termList.insert(pair<int, Term>(termId,t));
			        		  TID2Importance tId2Importance;
			        		  tId2Importance.id = termId++;
			        		  tId2Importance.importance = tfidfScore;
			        		  tID2ImportanceList.push_back(tId2Importance);

			 			  //}
			       	 }
			    }
		}
	  outwstfidf.close();
//	  outwsidf.close();
//	  outtermlist.close();
	  cout << "-------------Finishing Terminology List -----------" << endl;
	  return pair<map<int, Term>, vector<TID2Importance>>(termList, tID2ImportanceList);
}



vector<TuplePair> DocumentFilter::detectTags(Term& term){
	   vector<TuplePair> tuplePairs= postagger_->tag(term.value);
	   for(int i=0; i<tuplePairs.size(); i++){
	 	 term.pos_tags.push_back((string)tuplePairs[i].value);
	   }
	  return tuplePairs;
}



/**
 * e.g. filter based on pos tagging of word sequences
 */
vector<Term> DocumentFilter::filterByLingusticProps(vector<Term> terminologyList){
	vector<Term> filteredTerms;
	for(int i=0; i<terminologyList.size(); i++){
		vector<TuplePair> tuplePair = detectTags(terminologyList[i]);
		if (terminologyList[i].areAllPosTagsAcceptable(suppotedPOSTags)){
			filteredTerms.push_back(terminologyList[i]);
		}
	}
	return filteredTerms;
}

void DocumentFilter::analyzeWordCoOccurrences(){
	map<string, TargetWord>	singleWordOccurrenceMap;
	if(wordsOccurrenceMap.empty()){
		return;
	}

	long threshhold_minNrOfOccurrences = 200;
	long N_total = 0;

	for(map<string, TargetWord>::iterator	wordsOccurrenceMapIterator = wordsOccurrenceMap.begin(); wordsOccurrenceMapIterator != wordsOccurrenceMap.end(); wordsOccurrenceMapIterator++){
		string keywordstr = wordsOccurrenceMapIterator->first;
		vector<string> keywords;
		split(keywords, keywordstr, boost::is_any_of(" \t "));
		if(keywords.size() > 0){
			string kw = keywords[0];
			map<int,list<pair<int,int>>> docs = wordsOccurrenceMapIterator->second.docs;
			if(singleWordOccurrenceMap[kw].getTotalNrOfOccurrences() <= 0){
				TargetWord tw;
				singleWordOccurrenceMap[kw] = tw;
			}
			for(map<int,list<pair<int,int>>>::iterator docsiter = docs.begin(); docsiter != docs.end(); docsiter++){
				int docId = docsiter->first;
				list<pair<int,int>> positions = docsiter->second;
				for(list<pair<int,int>>::iterator positionsiter = positions.begin(); positionsiter != positions.end(); positionsiter++){
					if(singleWordOccurrenceMap[kw].getTotalNrOfOccurrences(docId) > 0){
						singleWordOccurrenceMap[kw].addDoc(docId, positionsiter->first, positionsiter->second);
					}else{
						singleWordOccurrenceMap[kw].addNewDoc(docId, positionsiter->first, positionsiter->second);
					}
				}
			}
		}
	}

	// calculating N_total
	for(map<string, TargetWord>::iterator singleWordOccurrenceMapIter = singleWordOccurrenceMap.begin(); singleWordOccurrenceMapIter != singleWordOccurrenceMap.end(); singleWordOccurrenceMapIter++ ){
		long N_total_word = singleWordOccurrenceMapIter->second.getTotalNrOfOccurrences();
		if(N_total_word > threshhold_minNrOfOccurrences){
			N_total += singleWordOccurrenceMapIter->second.getTotalNrOfOccurrences();
		}
	}

	map<string, WordPair> wps;
	// calculating mutual information of co-occurrence
	for(map<string, TargetWord>::iterator	wordsOccurrenceMapIterator = wordsOccurrenceMap.begin(); wordsOccurrenceMapIterator != wordsOccurrenceMap.end(); wordsOccurrenceMapIterator++){
		string keywordstr = wordsOccurrenceMapIterator->first;
		vector<string> keywords;
		split(keywords, keywordstr, boost::is_any_of(" \t"));
		if(keywords.size() == 2){
			WordPair wp;
			wp.word1 = singleWordOccurrenceMap[keywords[0]];
			wp.word2 = singleWordOccurrenceMap[keywords[1]];
			long nrOccurrWord1 = singleWordOccurrenceMap[keywords[0]].getTotalNrOfOccurrences();
			long nrOccurrWord2 = singleWordOccurrenceMap[keywords[1]].getTotalNrOfOccurrences();
			long nrOccurrWordPair = wordsOccurrenceMapIterator->second.getTotalNrOfOccurrences();
			if(nrOccurrWord2 == 0){
				cerr << keywords[1] << " has occurrence score 0" << endl;
				nrOccurrWord2 = 1;
			}
			wp.mutualOccurrence = log10((N_total*nrOccurrWordPair)/(nrOccurrWord1*nrOccurrWord2));
			wps[keywordstr] = wp;
		}
	}
}



pair< map<int, Term>, vector<TID2Importance> > DocumentFilter::calculateValue (vector<Term>& termCandidates, const int termSpecificity, const int MaxTermLen,const int totalNrOfDocs){

	 map<int, Term> termList;
	 vector<TID2Importance> tID2ImportanceList;
	 map<string, CValueTrip> cvalueTriple; // used for calculating c-value
	 int termId = 1;
	 std::sort(termCandidates.begin(), termCandidates.end(), myfunction);

		if (ScoreOn&1) { cerr << "Tfidf active..." << endl; }
		if (ScoreOn&2) { cerr << "cvalue active..." << endl;}
		if (ScoreOn&4) { cerr << "ncvalue active..." << endl;}
		if (ScoreOn&8) { cerr << "wikiscore active..." << endl;}
		if (ScoreOn&16) { cerr << "feature score active..." << endl;}

	 for (int i=0; i<termCandidates.size(); ++i) {

		 string term = termCandidates[i].value;

		 //TFIDF dump; dump.trimWord(term);  // remove the end space

		 int length = termCandidates[i].length();
		 double freqFromInput = wordsOccurrenceMap[term].getTotalNrOfOccurrences();
		 map<int,list<pair<int,int>>> docs = wordsOccurrenceMap[term].docs;

		 Word word_struct; word_struct.value = term;

		 // look for the term in the database
		 double wordsOccurenceInDatabase=0; double docNrOfTermInDatabase=0;
		 // for German
		 Term tmp;
		 string termNoUmlaut = tmp.removeUmlaut(term);
		 std::transform(termNoUmlaut.begin(), termNoUmlaut.end(), termNoUmlaut.begin(), ::tolower);

		 if(databaseWordsOccurenceMap.find(termNoUmlaut)!=databaseWordsOccurenceMap.end()) {
			 wordsOccurenceInDatabase = databaseWordsOccurenceMap[termNoUmlaut].first;
			 docNrOfTermInDatabase = databaseWordsOccurenceMap[termNoUmlaut].second;
		 }

		 double freq;
		 if (BigFreq) { freq = freqFromInput + wordsOccurenceInDatabase; }
		 else { freq = freqFromInput; }

		// filtering
		//if (freq <= termSpecificity) continue;
		if (word_struct.isNumber())  continue;
		if (term.size()<=1)	continue;


		 // calculating tfidf scores
		vector<double> tfScores;
		double tfScore = tfidf->tf(term, freq); // use of the overall freq of all input files
		tfScores.push_back(tfScore);

		 //calculate idf score
		 double idfScore = tfidf->idf(term, docs.size()+docNrOfTermInDatabase, totalNrOfDocs+databaseDocTotalCount);

		 double tfidfScore = tfidf->tfidf(term, tfScores, idfScore);

		 termCandidates[i].tfidf = tfidfScore;

		 // calculate wiki score
		 string termtmp=term;
		 std::transform(termtmp.begin(), termtmp.end(), termtmp.begin(), ::tolower);
		 if (wikiList.find(termtmp)!=wikiList.end()) {termCandidates[i].wikiscore = 1.0;}


		 termCandidates[i].freq = freq;
		 termCandidates[i].docN =  docs.size();
		 termCandidates[i].freqDatabase = wordsOccurenceInDatabase;
		 termCandidates[i].docNDatabase = docNrOfTermInDatabase;
		 termCandidates[i].docTotal = totalNrOfDocs;
		 termCandidates[i].docTotalDatabase = databaseDocTotalCount;
		 termCandidates[i].tfscore = tfScore;
		 termCandidates[i].idfscore = idfScore;


		// calculate C-Value score
		if ( length == MaxTermLen) {
				termCandidates[i].cvalue = log(length)/log(2)*freq;
				termCandidates[i].calculateNCValue(NCWeightsBefore, NCWeightsAfter);
				// add to term list;

				termCandidates[i].docs.insert(wordsOccurrenceMap[term].docs.begin(),wordsOccurrenceMap[term].docs.end());
				double importanceScore = termCandidates[i].calcImportanceScore(ScoreOn);
				termList.insert(pair<int,Term> (termId, termCandidates[i]));
				TID2Importance tId2Importance;
				tId2Importance.id = termId++;
				tId2Importance.importance = importanceScore;
				tID2ImportanceList.push_back(tId2Importance);
				/*\
				if (termCandidates[i].cvalue > threshold){
					add termCandidates[i] to output list
				} */
				reviseSubstring(term, cvalueTriple); // check and update all subStrings of term
		}
		else {
				if(cvalueTriple.find(term)==cvalueTriple.end()) { // term appears for the first time
					termCandidates[i].cvalue = log(length)/log(2)*freq;
					termCandidates[i].calculateNCValue(NCWeightsBefore, NCWeightsAfter);

					// add to term list;
					termCandidates[i].docs.insert(wordsOccurrenceMap[term].docs.begin(),wordsOccurrenceMap[term].docs.end());
					double importanceScore = termCandidates[i].calcImportanceScore(ScoreOn);
					termList.insert(pair<int,Term> (termId, termCandidates[i]));

					TID2Importance tId2Importance;
					tId2Importance.id = termId++;
					tId2Importance.importance = importanceScore;
					tID2ImportanceList.push_back(tId2Importance);
				}
				else {
					CValueTrip a = cvalueTriple[term];
					if (freq != a.f) {
						cerr << "Warning: term frequency does not equal ("<<freq<<"/"<<a.f<< ")" << endl;
					}
					termCandidates[i].cvalue = log(length)/log(2)*(freq-a.t/a.c);
					if (scoreDebug==1){termCandidates[i].a_t=a.t; termCandidates[i].a_c=a.c;}
					termCandidates[i].a_t=a.t; termCandidates[i].a_c=a.c;
					termCandidates[i].calculateNCValue(NCWeightsBefore, NCWeightsAfter);
					// add to term list;

					termCandidates[i].docs.insert(wordsOccurrenceMap[term].docs.begin(),wordsOccurrenceMap[term].docs.end());
					double importanceScore = termCandidates[i].calcImportanceScore(ScoreOn);
					termList.insert(pair<int,Term> (termId, termCandidates[i]));

					TID2Importance tId2Importance;
					tId2Importance.id = termId++;
					tId2Importance.importance = importanceScore;
					tID2ImportanceList.push_back(tId2Importance);

				}
				/*\
				if (termCandidates[i].cvalue > threshold){
					add termCandidates[i] to output list
				} */
				reviseSubstring(term,cvalueTriple); // check and update all subStrings of term
		}

	 }
	 string filePathTFIDFilteredWS = "../cvalueTriples"; // output file
	 ofstream outws;
	 outws.open(filePathTFIDFilteredWS.c_str());
	 	for (map<string, CValueTrip>::iterator it=cvalueTriple.begin(); it!=cvalueTriple.end(); ++it)
	 		outws << it->first << endl;

		cout << "-------------Finishing Terminology List -----------" << endl;
		return pair<map<int, Term>, vector<TID2Importance>>(termList, tID2ImportanceList);

}



pair< map<int, Term>, vector<TID2Importance> > DocumentFilter::calculateCValue (vector<Term>& termCandidates, int termSpecificity, const int MaxTermLen){

	map<int, Term> termList;
	vector<TID2Importance> tID2ImportanceList;
	int termId = 1;
	 map<string, CValueTrip> cvalueTriple; // used for calculating c-value


	std::sort(termCandidates.begin(), termCandidates.end());

	for (int i=0; i<termCandidates.size(); ++i) {

		string term = termCandidates[i].value; // remove the end space?
		TFIDF dump; dump.trimWord(term);

		int length = termCandidates[i].length();
		double freq = wordsOccurrenceMap[term].getTotalNrOfOccurrences();


		Word word_struct; word_struct.value = term;

		if (word_struct.isNumber())  continue;
		if (term.size()<=1)	continue;


		if ( length == MaxTermLen) {
			termCandidates[i].cvalue = log(length)/log(2)*freq;

			// add to term list;

			termCandidates[i].docs.insert(wordsOccurrenceMap[term].docs.begin(),wordsOccurrenceMap[term].docs.end());
			double importanceScore = termCandidates[i].calcImportanceScore(ScoreOn);
			termList.insert(pair<int,Term> (termId, termCandidates[i]));
			TID2Importance tId2Importance;
			tId2Importance.id = termId++;
			tId2Importance.importance = importanceScore;
			tID2ImportanceList.push_back(tId2Importance);


			reviseSubstring(term, cvalueTriple); // check and update all subStrings of term
		}
		else {
			if(cvalueTriple.find(term)==cvalueTriple.end()) { // term appears for the first time
				termCandidates[i].cvalue = log(length)/log(2)*freq;

				// add to term list;
				termCandidates[i].docs.insert(wordsOccurrenceMap[term].docs.begin(),wordsOccurrenceMap[term].docs.end());
				double importanceScore = termCandidates[i].calcImportanceScore(ScoreOn);
				termList.insert(pair<int,Term> (termId, termCandidates[i]));

				TID2Importance tId2Importance;
				tId2Importance.id = termId++;
				tId2Importance.importance = importanceScore;
				tID2ImportanceList.push_back(tId2Importance);
			}
			else {
				CValueTrip a = cvalueTriple[term];
				if (freq != a.f) {
					cerr << "Warning: term frequency does not equal ("<<freq<<"/"<<a.f<< endl;
				}
				termCandidates[i].cvalue = log(length)/log(2)*(freq-a.t/a.c);

				// add to term list;

				termCandidates[i].docs.insert(wordsOccurrenceMap[term].docs.begin(),wordsOccurrenceMap[term].docs.end());
				double importanceScore = termCandidates[i].calcImportanceScore(ScoreOn);
				termList.insert(pair<int,Term> (termId, termCandidates[i]));

				TID2Importance tId2Importance;
				tId2Importance.id = termId++;
				tId2Importance.importance = importanceScore;
				tID2ImportanceList.push_back(tId2Importance);
			}

			reviseSubstring(term,cvalueTriple); // check and update all subStrings of term
		}

	}

	cout << "-------------Finishing Terminology List -----------" << endl;
	return pair<map<int, Term>, vector<TID2Importance>>(termList, tID2ImportanceList);
}



void DocumentFilter::printWordCoOccurrences(map<string, TargetWord>	singleWordOccurrenceMap, map<string, WordPair>	wps){
	ofstream out;
	string outputFile = docDir+"/generated/word-co-occurrences.out";
	out.open(outputFile.c_str());

	cout << ".....Starting to analyze Word Co-Occurrences....." << endl;
	cout << "Word 1\tWord 2\tCo-Occurrences"<< endl;
	out << "KeyWord\tWord-1\tWord-1-Occurrence\tWord-2\tWord-2-Occurrence\tCo-Occurrences\tMutual-Occurrence" << endl;

	for(map<string, WordPair>::iterator wpsiterator = wps.begin(); wpsiterator != wps.end(); wpsiterator++){
		string keywordstr = wpsiterator->first;
		WordPair wp = wpsiterator->second;
		vector<string> keywords;
		split(keywords, wpsiterator->first, boost::is_any_of(" \t"));
		long nrOccurrWordPair = 0;

		map<int,list<pair<int,int>> > docs = wordsOccurrenceMap[keywordstr].docs;
		for(map<int,list<pair<int,int>> >::iterator docsiter = docs.begin(); docsiter != docs.end(); docsiter++){
			nrOccurrWordPair += docsiter->second.size();
		}
		out << keywordstr << "\t" <<  keywords[0] << "\t" << singleWordOccurrenceMap[keywords[0]].getTotalNrOfOccurrences() << "\t" << keywords[1] << "\t" << singleWordOccurrenceMap[keywords[1]].getTotalNrOfOccurrences() <<  "\t" << nrOccurrWordPair << "\t" << wp.mutualOccurrence << endl;
	}
	out.close();
}

void DocumentFilter::printMostFrequentWords(const char* path){
   ofstream out;
   out.open(path);
   if(!out.eof()){
	   for(map<int, double>::iterator docId2MostFreqWordNrOccurIter = docId2MostFrequentWordNr.begin(); docId2MostFreqWordNrOccurIter != docId2MostFrequentWordNr.end(); docId2MostFreqWordNrOccurIter++){
		   int docId = docId2MostFreqWordNrOccurIter->first;
		   double freq = docId2MostFreqWordNrOccurIter->second;
		   out << docId << " " << freq << "\n";
	   }
   }
   out.close();
}

void DocumentFilter::printWordOccurrence(const char* path){
	ofstream out;
	out.open(path);
	if(!out.eof()){
		for(map<string, TargetWord>::iterator wordsOccurrenceMapIter = wordsOccurrenceMap.begin(); wordsOccurrenceMapIter != wordsOccurrenceMap.end(); wordsOccurrenceMapIter++){
			map<int,list<pair<int,int>> > docs = wordsOccurrenceMapIter->second.docs;
			for(map<int, list<pair<int,int>> >::iterator docsiter = docs.begin(); docsiter != docs.end(); docsiter++){
				out << wordsOccurrenceMapIter->first << " " << docsiter->first << " " << docsiter->second.size()  << "\n";
			}
		}
	}
	out.close();
}

void DocumentFilter::reviseSubstring(string term, map<string, CValueTrip>& cvalueTriple){
	vector<string> words;
	istringstream item(term);
	string w;
	while (item >> w) {words.push_back(w); w="";}

	for (int i=0; i<words.size(); ++i){
		string s="";
		for (int j=i; j<words.size(); ++j){
			s=s+words[j]+" ";

			if (j-i+1 < words.size()){ // only for substrings
				string tmps = s;
				TFIDF dump; dump.trimWord(tmps);
				if (cvalueTriple.find(tmps) == cvalueTriple.end()) {

					 // look for the term in the database
					 double sOccurenceInDatabase=0;
					 Term tmp;
					 string sNoUmlaut = tmp.removeUmlaut(tmps);
					 if(databaseWordsOccurenceMap.find(sNoUmlaut)!=databaseWordsOccurenceMap.end()) {
						 sOccurenceInDatabase = databaseWordsOccurenceMap[sNoUmlaut].first; }

					 double termOccurenceInDatabase=0;
					 string termNoUmlaut = tmp.removeUmlaut(term);
					 if(databaseWordsOccurenceMap.find(termNoUmlaut)!=databaseWordsOccurenceMap.end()) {
						 termOccurenceInDatabase = databaseWordsOccurenceMap[termNoUmlaut].first; }

					CValueTrip triple;
					if(BigFreq) {triple.set(wordsOccurrenceMap[tmps].getTotalNrOfOccurrences()+sOccurenceInDatabase,wordsOccurrenceMap[term].getTotalNrOfOccurrences()+termOccurenceInDatabase,1);}
					else {triple.set(wordsOccurrenceMap[tmps].getTotalNrOfOccurrences(), wordsOccurrenceMap[term].getTotalNrOfOccurrences(),1);}

					cvalueTriple.insert(make_pair(tmps,triple));
				}
				else {
					cvalueTriple[tmps].c=+1;
					cvalueTriple[tmps].t=+cvalueTriple[term].f-cvalueTriple[term].t;
				}
			}
		}
	}
	words.clear();
}

bool DocumentFilter::isValid(Term t, string language){
/*check term boundaries and stop words*/
	if (inBlackList(t,language)) {return 0;}
	if (t.isBadStartOrEndorMiddle(language)) {return 0;}
	return 1;

}

bool DocumentFilter::inBlackList(Term t, string language){

	string lowercase = boost::to_lower_copy(t.value);
	if ((language=="de")||(language=="DE"))  lowercase=t.removeUmlaut(lowercase);

	if (termBlackList.find(lowercase) != termBlackList.end()) {return 1;}
	return 0;
}

void DocumentFilter::loadDocsTotalCountFromDatabase(const string databaseDocPath) {
	   ifstream in;
	   in.open(databaseDocPath.c_str());

	   string line;
	   getline(in,line);
	   istringstream item(line);
	   item >> databaseDocTotalCount;

	   in.close();
	   cerr << "databaseDocTotalCount: " << databaseDocTotalCount << endl;
}

void DocumentFilter::addDocsTotalCountFromDatabase(const string databaseDocPath) {
	   ifstream in;
	   in.open(databaseDocPath.c_str());

	   int docCount;
	   string line;
	   getline(in,line);
	   istringstream item(line);
	   item >> docCount;
	   databaseDocTotalCount=databaseDocTotalCount+docCount;

	   in.close();
	   cerr << "databaseDocTotalCount: " << databaseDocTotalCount << endl;
}


void DocumentFilter::loadWordOccurrenceFromDatabase(const string DatabaseNgramsPath) {
	ifstream in;
	cerr << "Loading Database " << DatabaseNgramsPath << endl;
	in.open(DatabaseNgramsPath.c_str());

	string line; double count, docNr;
	while(getline(in,line)) {
		string word_composition;
		istringstream item(line);
		item >> count;
		item >> docNr;

		string word;
		while(item>>word){
			word_composition = word_composition+word+" ";
		}
		TFIDF dump; dump.trimWord(word_composition);
		pair<double, double> tmp = make_pair(count, docNr);
		Term tmpterm; tmpterm.value = word_composition;
		if (isValid(tmpterm, "en")) databaseWordsOccurenceMap.insert(make_pair(word_composition,tmp));
	}

	in.close();

	cerr << "DataBase's size: "<< databaseWordsOccurenceMap.size() << " items" << endl;
}

void DocumentFilter::addWordOccurrenceFromDatabase(const string DatabaseNgramsPath) {
	ifstream in;
	cerr << "Loading additional Database " << DatabaseNgramsPath << endl;
	in.open(DatabaseNgramsPath.c_str());

	string line; double count, docNr;
	while(getline(in,line)) {
		string word_composition;
		istringstream item(line);
		item >> count;
		item >> docNr;

		string word;
		while(item>>word){
			word_composition = word_composition+word+" ";
		}

		TFIDF dump; dump.trimWord(word_composition);

		if (databaseWordsOccurenceMap.find(word_composition) != databaseWordsOccurenceMap.end()){
			databaseWordsOccurenceMap[word_composition].first+=count;
			databaseWordsOccurenceMap[word_composition].second+=docNr;
		}
		else {
		pair<double, double> tmp = make_pair(count, docNr);
		databaseWordsOccurenceMap.insert(make_pair(word_composition,tmp));
		}
	}

	in.close();

	cerr << "DataBase's size: "<< databaseWordsOccurenceMap.size() << " items" << endl;

}


void DocumentFilter::loadNCWeights(const string beforeName, const string afterName){
	ifstream in;
	cerr << "Loading weights of context words from: " << beforeName << "and" << afterName<< endl;
	string line, word, separator; double weight, count;

	in.open(beforeName.c_str());
	while(getline(in,line)) {
		istringstream item(line);
		item >> word;
		item >> separator;
		item >> weight;
		item >> separator;
		item >> count;
		NCWeightsBefore.insert(pair<string, double>(word,weight));
	}
	in.close();

	in.open(afterName.c_str());
	while(getline(in,line)) {
		istringstream item(line);
		item >> word;
		item >> separator;
		item >> weight;
		item >> separator;
		item >> count;
		NCWeightsAfter.insert(pair<string, double>(word,weight));
	}
	in.close();

	cerr << "load "<<NCWeightsBefore.size() << " weights(Before) and " <<  NCWeightsAfter.size() << " weights(After)" << endl;
}

std::string DocumentFilter::exec(const char* cmd) {
    FILE* pipe = popen(cmd, "r");
    if (!pipe) return "ERROR";
    char buffer[128];
    std::string result = "";
    while(!feof(pipe)) {
    	if(fgets(buffer, 128, pipe) != NULL)
    		result += buffer;
    }
    pclose(pipe);
    return result;
}

vector<Term> DocumentFilter::removeShorterOneInSimilarCandidates(vector<Term> termCandidates){

	string allContent="";
	vector<Term> termLists;

	// create a word string including all term candidates, separated with "#"
	for (int i=0; i<termCandidates.size(); ++i){
		string value = termCandidates[i].value;
		std::transform(value.begin(), value.end(), value.begin(), ::tolower);
		allContent=allContent+value+"#";
	}

	for (int i=0; i<termCandidates.size(); ++i){
		string termlc = termCandidates[i].value;
		std::transform(termlc.begin(), termlc.end(), termlc.begin(), ::tolower);
		int occurTimes = findString(allContent,termlc);
		if (occurTimes==1)// occurs only once; there is not longer candidates including this term
		{Term keepterm(termCandidates[i]); termLists.push_back(termCandidates[i]);}
	}

	return vector<Term>(&termLists[0], &termLists[termLists.size()]);
}

int DocumentFilter::findString (string longStr, string s){

	int times=0;
	size_t pos = longStr.find(s);

	while (pos!=string::npos) {
		times++;
		pos=longStr.find(s, pos+1);
	}
	return times;
}
