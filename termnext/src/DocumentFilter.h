/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SENTENCEFILTER_H_
#define SENTENCEFILTER_H_

#include "TFIDF.h"
#include "Term.h"
#include <string.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include <map>
#include <boost/algorithm/string.hpp>
#include "boost/iterator/transform_iterator.hpp"
#include "PythonConnector.h"
#include "lapos-0.1.2/postagger.h"
#include "CNCValue.h"


using namespace std;
using namespace boost;

typedef map<string,TargetWord>::key_type (*get_key_t)(map<string,TargetWord>::value_type);
typedef map<string,TargetWord>::iterator map_iterator;
typedef boost::transform_iterator<get_key_t, map_iterator> mapkey_iterator;


class DocumentFilter {
public:
	vector<char*> allCountries;
	vector<string> suppotedPOSTags;
	postagger* postagger_;
	//PythonConnector pConnector;

	map<string ,map<string, double> >supportedPosPatterns;
	vector<vector<TuplePair> > units; //units of doc: key: word; value:pos tag;

	DocumentFilter(vector<string> suppotedPOSTags, const char* docDir, const char* modelpath, vector<char*> allCountries, bool excludeStopWords, char** neStopWords, int size, Gramm ngramm);
	DocumentFilter();
	virtual ~DocumentFilter();
	void init(vector<string> suppotedPOSTags, vector<char*> allCountries, bool excludeStopWords);
	void reset();
/*	void initPython();
	void exitPython();*/
	void loadNEStopWords(const string filename);
	pair< map<int, Term>, vector<TID2Importance> > filterByTFIDFCombiningSingleWords(int termSpecificity, int totalNrOfDocs);
	void filterByTFIDFConsideringWordSequences(Gramm ngamm);
	vector<Term> filterByLingusticProps(vector<Term> terminologyList);
	string filterByRSI(string sentence);
	void calculateWordOccurrence(stringstream& datastream, int docCounter, Gramm ngram);
	void analyzeWordCoOccurrences();
	vector<TuplePair> detectTags(Term& term);
	void printFilteredWord(ostream& out, ostream& outws, string word, double tfidfScore);
	void printWordOccurrence(const char* path);
	void printMostFrequentWords(const char* path);
	void printWordCoOccurrences(map<string, TargetWord>	singleWordOccurrenceMap, map<string, WordPair> wps);
	string getNGramm(Gramm ngramm, map<string, TargetWord>::iterator& i, map<string, TargetWord>::iterator keyend);
	const char* getWordStamm(string word);

	int loadPosPatterns(const string filename, string language);
	void postagging(stringstream& datastream, string language);
	vector<Term> filterByPOSPatterns(const int MaxTermLen, const string language);
	pair< map<int, Term>, vector<TID2Importance> > calculateCValue (vector<Term>& termCandidates, const int termSpecificity, const int MaxTermLen);
	pair< map<int, Term>, vector<TID2Importance> > calculateValue (vector<Term>& termCandidates, const int termSpecificity, const int MaxTermLen,const int totalNrOfDocs);
	void calculateNCValue (vector<Term>& termCandidates, const int contextWin, const bool posfilter);
	vector<Term> filterByPOSPatterns(vector<Term> termList);
	void filterByTFIDF(vector<Term>& termCandidates, const int totalNrOfDocs);
	vector<Term> removeShorterOneInSimilarCandidates(vector<Term> termCandidates);

	bool isValid(Term t, string language);
	int  loadTermBlackList(const string filename);
	bool inBlackList(Term t, string language);

	void loadNCWeights(const string beforeName, const string afterName);
	int  loadWiKi(const string filename, const string language);

	void loadDocsTotalCountFromDatabase(const string databaseDocpath);
	void loadWordOccurrenceFromDatabase(const string DatabaseNgramsPath);
	void addDocsTotalCountFromDatabase(const string databaseDocpath);
	void addWordOccurrenceFromDatabase(const string DatabaseNgramsPath);


private:
	set<string> termBlackList;
	set<string> wikiList;

	void reviseSubstring(string term, map<string, CValueTrip>& cvalueTriple);
	map<string, double> NCWeightsBefore, NCWeightsAfter; // store the context words' weights for NC-Score
	string exec(const char* cmd);

	int findString(string content, string s); // look for how many time of s occuring in content

};


#endif /* SENTENCEFILTER_H_ */
