/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ExtractCountries.h"

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <boost/algorithm/string.hpp>

#define allCountriesConst "List of cities in "

boost::regex allCountriesRegex("(" + (string)allCountriesConst +"[A-Z][a-zA-Z\\s]*)");


ExtractCountries::ExtractCountries() {
}

ExtractCountries::~ExtractCountries() {
}

vector<char*> ExtractCountries::extractAllCountries(const char* allCountriesPath) {
	vector<char*> allCountries;
	int startPos = strlen(allCountriesConst);

	boost::smatch sm;
	ifstream in;
	in.open(allCountriesPath);

	string line;
	while (getline(in, line) != NULL) {
		boost::sregex_token_iterator iter(line.begin(), line.end(), allCountriesRegex, 0);
		boost::sregex_token_iterator end;
		for ( ; iter != end; ++iter) {
			string listOfCitiesIn = (*iter);

			string country = listOfCitiesIn.substr(startPos, listOfCitiesIn.size());
			to_lower(country);
			if(country.size() > 1){
				if(find(allCountries.begin(), allCountries.end(), country) == allCountries.end()){
					allCountries.push_back(strdup(country.c_str()));
				}
			}
		}
	}
	in.close();
	return allCountries;
}

/*int main(int argc, char **argv) {
	ExtractCountries ec;
	ec.extractAllCountries(
			"/home/nkokhlik/Documents/EU-Bridge/Lists_of_cities_by_country.json");
}*/
