/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    This software was developed by

    the Karlsruhe Institute of Technology
        Institute for Anthropomatics and Robotics
        Adenauerring 2
        76131 Karlsruhe
        Germany

    

   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef EXTRACTCOUNTRIES_H_
#define EXTRACTCOUNTRIES_H_

#include <boost/regex.hpp>

using namespace std;
using namespace boost;

class ExtractCountries {
public:
	ExtractCountries();
	virtual ~ExtractCountries();
	vector<char*> extractAllCountries(const char* listOfCountriesPath);
};

#endif /* EXTRACTCOUNTRIES_H_ */
