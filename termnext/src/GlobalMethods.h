/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <dirent.h>

#include "FilePathNode.h"

#ifndef GLOBALMETHODS_H_
#define GLOBALMETHODS_H_
#define MAX_NGRAM 5


enum Gramm{
	Uni=1,
	Bi=2,
	Tri=3,
	Four=4,
	Five=5
};

inline Gramm getNGramm(int code){
	switch(code){
		case 1:
			return Uni;
		case 2:
			return Bi;
		case 3:
			return Tri;
		case 4:
			return Four;
		case 5:
			return Five;
		default:
			return Five;
	}
}

//#ifndef ALL_FILE_NAMES_
//#define ALL_FILE_NAMES_
inline pair<FilePathNode*, int> readAllFileNames(string documentDir){
	FilePathNode* filePathNode = 0;
	int tNrOfDocs = 0;
	DIR* dir = opendir(documentDir.c_str());
	FilePathNode* current;
	int id = 0;
	if(dir){
		struct dirent* ent = readdir(dir);
		while((ent = readdir(dir))){
			string d_name = ent->d_name;
			cout << d_name << endl;
			if(d_name.empty() || ent->d_type != DT_REG){
				continue;
			}
			if(!filePathNode){
				filePathNode = new FilePathNode;
				string fileName = documentDir+"/"+d_name;
				filePathNode->data = fileName;
				filePathNode->id = id;
				current = filePathNode;
				current->next = 0;
			}else{
				string fileName = documentDir+"/"+d_name;
				FilePathNode* subNode = new FilePathNode;
				subNode->data = fileName;
				subNode->id = id;
				subNode->next = 0;
				current->next = subNode;
				current = subNode;
				subNode->next = 0;
			}
			id++;
			tNrOfDocs++;
		}
		closedir(dir);
	}
	return pair<FilePathNode*, int>(filePathNode, tNrOfDocs);
}
#endif
