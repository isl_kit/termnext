/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "PythonConnector.h"
#include <stdarg.h>

PythonConnector::PythonConnector() {
}

PythonConnector::~PythonConnector() {
}

PyThreadState *mainstate;

PyObject* PythonConnector::initNLTK(){
	  PyObject *pName = PyUnicode_FromString("nltk");
	  PyObject *pModule = PyImport_Import(pName);
	  Py_DECREF(pName);
	  return pModule;
}


PyObject* PythonConnector::prepareForNLTKQueries(PyObject* pModule){
	  PyObject* dictionary = PyModule_GetDict(pModule);
	  assert(dictionary);
	  return dictionary;
}


PyObject* PythonConnector::borrowMethodReference(PyObject* dictionary, const char* method_name){
	  PyObject *method_ref
		     = PyDict_GetItemString(dictionary, method_name);
	  return method_ref;
}

PyObject* PythonConnector::callMethod(PyObject* methodRef, PyObject* args){
	 PyObject* pResult =PyObject_CallObject(methodRef, args);
	 return pResult;
}

PyObject* PythonConnector::getAsTuple( PyObject* pResult){
	PyObject * list = PyList_AsTuple(pResult);
	return list;
}

PyObject* PythonConnector::createStringArgumentObject(PyObject* method_ref, int nrofargs, ...){
	  va_list args;
	  va_start(args, nrofargs);

	  PyObject* pArgsPos;
	  pArgsPos = PyTuple_New(1);

	  for(int i=0; i<nrofargs; i++){
		  string val=va_arg(args, char*);
		  PyObject *pValue = PyString_FromString(val.c_str());
		  PyTuple_SetItem(pArgsPos, i, pValue);

	  }

	  return pArgsPos;
}

/*
TuplePair PythonConnector::extractPairFromTuple(PyObject* list, int index){
	   TuplePair tp;
	   PyObject* item = PySequence_GetItem(list, index);
	   char* key =  PyString_AsString(PyTuple_GetItem(item, 0));
	   char* value =  PyString_AsString(PyTuple_GetItem(item, 1));
	   tp.key = key;
	   tp.value = value;

	   return tp;
}*/

int PythonConnector::length(PyObject * list){
	return  PySequence_Length(list);
}

void PythonConnector::deRefObject(PyObject* obj){
	Py_DECREF(obj);
}

void PythonConnector::initPython(){
    PyEval_InitThreads();
    Py_Initialize();
    mainstate = PyThreadState_Swap(NULL);
    PyEval_ReleaseLock();
}

void PythonConnector::exitPython(){
    PyEval_AcquireLock();
    PyThreadState_Swap(mainstate);
    Py_Finalize();
}
