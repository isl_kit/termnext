/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PYTHONCONNECTOR_H_
#define PYTHONCONNECTOR_H_
#include <Python.h>
#include <iostream>
#include <string.h>


using namespace std;

/*
struct TuplePair{
	char* key;
	char* value;
};
*/

class PythonConnector {

public:
	PythonConnector();
	virtual ~PythonConnector();
	PyObject* initNLTK();
	void initPython();
	void exitPython();
	PyObject* prepareForNLTKQueries(PyObject* pModule);
	PyObject* borrowMethodReference(PyObject* dictionary, const char* method_name);
	PyObject* callMethod(PyObject* methodRef, PyObject* args);
	PyObject* getAsTuple( PyObject* pResult);
	PyObject* createStringArgumentObject(PyObject* method_ref, int nrofargs, ...);
//	TuplePair extractPairFromTuple(PyObject* list, int index);
	int length(PyObject * list);
	void deRefObject(PyObject* obj);
};

#endif /* PYTHONCONNECTOR_H_ */
