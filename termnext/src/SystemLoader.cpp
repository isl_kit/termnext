/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "SystemLoader.h"
#include "ConfigLoader.h"

SystemLoader::SystemLoader() {
}

SystemLoader::~SystemLoader() {
}

void SystemLoader::init(const char* confFilePath){
	ConfigLoader confLoader;
	cout << "Initializing EU-Bridge config........................." << endl;
	int configloaded = confLoader.init(confFilePath);
	if(configloaded == 1){
		cout << "The configuration is successfully loaded" << endl;
		vector<Type> supportedNETypes = confLoader.loadNETypes();
		supportedPosTypes = confLoader.loadTermList();
		types.allTypes = supportedNETypes;
		cout << "The number of supported POS types: " << types.list().size() << endl;
		cout << "The number of named-entity types: " << supportedPosTypes.size() << endl;

		}else{
		cout << "The configuration failed to loaded" << endl;
	}
}

void SystemLoader::initpostagger(const char* modelpath){
	ptagger.loadModel(modelpath);
}
