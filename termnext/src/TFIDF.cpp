/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TFIDF.h"
#include "math.h"
#include <fstream>
#include <string.h>
#include <sstream>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <sstream>
#include <clocale>
#include <cstdlib>

using namespace boost;
using namespace std;

boost::regex trimCharachtersEnd("(.*)([!|,|;|.|:|?|\"|'|@|<|>])$");
boost::regex trimCharactersStart("^([!|,|;|.|:|?|\"|'|@|<|>])(.*)");

int MIN_WORD_LENGTH = 0;
bool excludeStopWords = false;

map<string, int> englishStopWords;

TFIDF::TFIDF(bool excludeStopWrds) {
	excludeStopWords = excludeStopWrds;
	loadEnglishStopWords();
//	flPathNode = flPathNode_;
//	totalNrOfAllWords = nrOfAllWords;
}

TFIDF::~TFIDF() {
}

void TFIDF::loadEnglishStopWords(){
	ifstream in;
	in.open("common-english-words.txt");
	string word;
	while(in >> word){
		englishStopWords[word] = 1;
	}
	in.close();
}

void TFIDF::removeDupplicates(vector<double>& elements){
	int newSize = 1;
	for(unsigned int i=0; i<elements.size(); i++){
		int j=0;
		while(j<newSize){
			if(elements[j] == elements[i]){
				break;
			}
			j++;
		}
		if (j >= newSize){
			elements[newSize++] = elements[i];
		}
	}
	elements.resize(newSize);
 }


void TFIDF::addWord(map<string, TargetWord>& targetWords, string word_composition, int id, int sposition, int eposition){

	trimWord(word_composition);
	if (word_composition=="") return;

	if(targetWords[word_composition].getTotalNrOfOccurrences() > 0){
		if(targetWords[word_composition].getTotalNrOfOccurrences(id) > 0){
			targetWords[word_composition].addDoc(id, sposition, eposition);
		}else{
			targetWords[word_composition].addNewDoc(id, sposition, eposition);
		}
	}else{
		TargetWord tw;
		tw.word = word_composition;
		tw.addNewDoc(id, sposition, eposition);
		targetWords[word_composition] = tw;
	}
}


void TFIDF::calculateWordOccurrenceByFile(const char* filePath, int id, map<string, TargetWord>& targetWords, map<int, double>& docId2MostFrequentWordNr, Gramm ngramm){
	ifstream ifstream_;
	ifstream_.open(filePath);
	stringstream strStream;
	strStream << ifstream_.rdbuf();
	calculateWordOccurrence(strStream, id, targetWords, docId2MostFrequentWordNr, ngramm);
	ifstream_.close();
}


void TFIDF::calculateWordOccurrence(iostream& data, int id, map<string, TargetWord>& targetWords, map<int, double>& docId2MostFrequentWordNr, Gramm ngramm){
// calculate the words frequency with 1 to ngramm words.
		vector<string> words; int wordId=-1;

		cout << "ngramm: " << ngramm << endl;
		string word_prev_prev_prev_prev = "";
		int pos_prev_prev_prev_prev =0;
	    string word_prev_prev_prev = "";
	    int pos_prev_prev_prev = 0;
		string word_prev_prev = "";
		int pos_prev_prev = 0;
		string word_prev = "";
		int pos_prev = 0;
		string word = "";
		int pos = 0;

		int offset=0;
		int currentPos;

		data.seekg(0,data.end);
		int dataLength = data.tellg();
		data.seekg(0, ios::beg);

		int word_composition_pos = 0;
		docId2MostFrequentWordNr[id] = 0;

		while(!data.eof() && data >> word){

						words.push_back(word); wordId++;
						offset=offset+(word.size()-utfStrLength(word));

						//pos = data.tellg() - utfStrLength(word);
						if(data.tellg()==-1) {currentPos = dataLength-offset;}
						else {currentPos = int (data.tellg()) - offset; }
						pos = currentPos - utfStrLength(word);
						//trimWord(word);
						if(word.size() == 0){
								continue;
						}
						if(isJunk(word)){
								continue;
						}
						//addWord(targetWords, word, id, pos, (int)data.tellg());
						addWord(targetWords, word, id, pos, currentPos);

						string word_composition = word;
						word_composition_pos = pos;
						double 	totalNrOfOccurrences = targetWords[word_composition].getTotalNrOfOccurrences(id);
						if(totalNrOfOccurrences >= docId2MostFrequentWordNr[id]){
							docId2MostFrequentWordNr[id] = totalNrOfOccurrences;
						}

						// for Bi-Gram
						word_composition = word_prev + " " + word;
						word_composition_pos = pos_prev;
						//trimWord(word_composition);
						if(word_composition.size() < 3){
						  pos = int (data.tellg())-offset;
								continue;
						}
						//addWord(targetWords, word_composition, id, word_composition_pos, (int)data.tellg());
						if((int)data.tellg()==-1){currentPos = dataLength-offset;}
						else {currentPos = int (data.tellg()) - offset; }
						addWord(targetWords, word_composition, id, word_composition_pos, currentPos );

					//	targetWords[word].print();
						totalNrOfOccurrences = targetWords[word_composition].getTotalNrOfOccurrences(id);
						if(totalNrOfOccurrences >= docId2MostFrequentWordNr[id]){
							docId2MostFrequentWordNr[id] = totalNrOfOccurrences;
						}


						// for Three-Gram
						word_composition = word_prev_prev + " " + word_composition;
						word_composition_pos = pos_prev_prev;
						//trimWord(word_composition);
						if(word_composition.size() < 3){
						  pos = int (data.tellg()) - offset;
								continue;
						}
						//addWord(targetWords, word_composition, id, word_composition_pos, (int)data.tellg());
						if((int)data.tellg()==-1){currentPos = dataLength-offset;}
						else {currentPos = int (data.tellg()) - offset; }
						addWord(targetWords, word_composition, id, word_composition_pos, currentPos);

						//	targetWords[word].print();
						totalNrOfOccurrences = targetWords[word_composition].getTotalNrOfOccurrences(id);
						if(totalNrOfOccurrences >= docId2MostFrequentWordNr[id]){
							docId2MostFrequentWordNr[id] = totalNrOfOccurrences;
						}


						// for Four-Gram
						word_composition = word_prev_prev_prev + " " + word_composition;
						word_composition_pos = pos_prev_prev_prev;
						//trimWord(word_composition);
						if(word_composition.size() < 3){
								pos = (int)data.tellg()-offset;
								continue;
						}
						//addWord(targetWords, word_composition, id, word_composition_pos, (int)data.tellg());
						if((int)data.tellg()==-1){currentPos = dataLength-offset;}
						else {currentPos = int (data.tellg()) - offset; }
						addWord(targetWords, word_composition, id, word_composition_pos, currentPos);

						//	targetWords[word].print();
						totalNrOfOccurrences = targetWords[word_composition].getTotalNrOfOccurrences(id);
						if(totalNrOfOccurrences >= docId2MostFrequentWordNr[id]){
							docId2MostFrequentWordNr[id] = totalNrOfOccurrences;
						}


						// for Five-Gram
						word_composition = word_prev_prev_prev_prev + " " + word_composition;
						word_composition_pos = pos_prev_prev_prev_prev;
						//trimWord(word_composition);
						if(word_composition.size() < 3){
								pos = (int)data.tellg()-offset;
								continue;
						}
						//addWord(targetWords, word_composition, id, word_composition_pos, (int)data.tellg());
						if((int)data.tellg()==-1){currentPos = dataLength-offset;}
						else {currentPos = int (data.tellg()) - offset; }
						addWord(targetWords, word_composition, id, word_composition_pos,currentPos);

						//	targetWords[word].print();
						totalNrOfOccurrences = targetWords[word_composition].getTotalNrOfOccurrences(id);
						if(totalNrOfOccurrences >= docId2MostFrequentWordNr[id]){
							docId2MostFrequentWordNr[id] = totalNrOfOccurrences;
						}

					word_prev_prev_prev_prev = word_prev_prev_prev;
					pos_prev_prev_prev_prev = pos_prev_prev_prev;
					word_prev_prev_prev = word_prev_prev;
					pos_prev_prev_prev = pos_prev_prev;
					word_prev_prev = word_prev;
					pos_prev_prev = pos_prev;
					word_prev = word;
					pos_prev = pos;
					word_composition = "";
				}
				//addWord(targetWords, word, id, word_composition_pos, (int)data.tellg());
				if((int)data.tellg()==-1){currentPos = dataLength-offset;}
				else {currentPos = int (data.tellg()) - offset; }
				addWord(targetWords, word, id, word_composition_pos, currentPos );


}



vector<Word2Occurrence>  TFIDF::findHighestScore(map<string, TargetWord> targetWords, double totalNrOfDocs){
	vector<Word2Occurrence> highestScore;
	map<string, TargetWord>::iterator targetWordsIterator = targetWords.begin();
	int index = 0;
	while(index < maxNumberOfElems){
		if (targetWordsIterator->second.word.size() < MIN_WORD_LENGTH){
			targetWordsIterator++;
			continue;
		}
		Word2Occurrence wo;
		wo.word = targetWordsIterator->second.word;

		wo.occurrence = targetWordsIterator->second.getTotalNrOfOccurrences();
		highestScore.push_back(wo);
		index++;
		targetWordsIterator++;
	}
	bool hasChanges = true;
	while(hasChanges){
		hasChanges = false;
		for(unsigned int i=0; i+1<highestScore.size(); i++){
			if(highestScore[i].occurrence < highestScore[i+1].occurrence){
				hasChanges = true;
				Word2Occurrence wo_ = highestScore[i];
				highestScore[i] = highestScore[i+1];
				highestScore[i+1] = wo_;
			}
		}
	}

	for(; targetWordsIterator != targetWords.end(); targetWordsIterator++){
		string word = targetWordsIterator->first;
		TargetWord targetWord = targetWordsIterator->second;
		if (word.size() < MIN_WORD_LENGTH){
			continue;
		}
		//TODO - > fix maxNumberOfElems
		if(targetWord.docs.size() < totalNrOfDocs){
			continue;
		}
		int counter = 0;
		int i = 0;
		while(i < maxNumberOfElems){
			double highestScore_ = highestScore[i].occurrence;
			if (targetWord.getTotalNrOfOccurrences() > highestScore_){
				break;
			}
			i++;
			counter++;
		}
		if(i < maxNumberOfElems){
			int i_=maxNumberOfElems-1;
			while(i_ > i){
				highestScore[i_] = highestScore[i_-1];
				i_--;
			}
			Word2Occurrence w2o;
			w2o.occurrence = targetWord.getTotalNrOfOccurrences();
			w2o.word = targetWord.word;
			highestScore[i] = w2o;
		}

	}
	return highestScore;
}

void TFIDF::trimWord(string& word){
//	to_lower(word);
	cmatch what;
	while (boost::regex_match(word.c_str(), what, trimCharachtersEnd) ){
		//word.find('.', word.size()-1) != string::npos || word.find('?', word.size()-1) != string::npos || word.find(',', word.size()-1) != string::npos || word.find(':', word.size()-1) != string::npos || word.find('"', word.size()-1) != string::npos){
		if(word.size() == 1){
			break;
		}
		word = word.substr(0, word.size()-1);
	}
	while (boost::regex_match(word.c_str(), what, trimCharactersStart)){
		if(word.size() == 1){
			break;
		}
		word = word.substr(1, word.size());
	}
	trim(word);
}

bool TFIDF::isJunk(string& word){
	int i=0;
	int nrOfChar = 0;
	while(i < word.length()){
		if (isalpha(word[i])||isdigit(word[i])){
			nrOfChar++;
		}
		i++;
	}
	return (word.length() >= nrOfChar*2);
}

double TFIDF::numberOfDocs(const char* documentDir){
	double nrOfDocs = 0;
	DIR* dir = opendir(documentDir);
	if(dir){
		struct dirent* ent ;
		while((ent = readdir(dir)) != NULL){
			nrOfDocs++;
		}
	}
	return nrOfDocs;
}

double TFIDF::numberOfSentences(const char* file){
	ifstream in;
	in.open(file);
	double numberOfSentences = 0;
	string line;
	while(!in.eof()){
		getline(in, line);
		numberOfSentences++;
	}
	in.close();
	return numberOfSentences;
}

double TFIDF::tf(string word, double nrOfTargetWord, double mostFrequentWordInDoc) {
	//augmented frequency, to prevent a bias towards longer documents, e.g. raw frequency divided by the maximum raw frequency of any term in the document:
	return (0.5+0.5*nrOfTargetWord/mostFrequentWordInDoc);
}
double TFIDF::tf(string word, double nrOfTargetWord) {
	return log10(nrOfTargetWord+1);
}

double TFIDF::idf(string word, double nrOfDocsWithTargetWord, double totalNrOfAllDocs) {
	//return (totalNrOfAllDocs==nrOfDocsWithTargetWord)?0.00000000001:(log10(totalNrOfAllDocs/nrOfDocsWithTargetWord));
	return log10(totalNrOfAllDocs/nrOfDocsWithTargetWord+1);
}

double TFIDF::tfidf(string word, vector<double> tfscores, double idfScore){
	double highestTfScore = 0;
	if(tfscores.size() == 0){
		//cerr << "No TF score available for " << word << ". IDF score is " << idfScore << ".\n";
		return -1;
	}
	for(int i=0; i<tfscores.size(); i++){
		if(tfscores[i] > highestTfScore){
			highestTfScore = tfscores[i];
		}
	}
	return highestTfScore*idfScore;
}

double TFIDF::augmentedTf(string word, double nrOfTargetWord, double mostFrequentWordInDoc){
	return 0.5 + 0.5*(nrOfTargetWord/mostFrequentWordInDoc);
}

double TFIDF::augmentedIdf(string word, double nrOfDocsWithTargetWord, double totalNrOfAllDocs) {
	double augmentedIdf = log10((totalNrOfAllDocs - nrOfDocsWithTargetWord)/nrOfDocsWithTargetWord);
	if(augmentedIdf > 0){
		return augmentedIdf;
	}
	return 0.0;
}

int TFIDF::utfStrLength(string str){
	  unsigned int strLen = str.length();

	  setlocale(LC_ALL, "en_EN.UTF-8");
	  unsigned int u = 0;
	  const char *c_str = str.c_str();
	  unsigned int charCount = 0;
	  while(u < strLen)
	  {
	    u += mblen(&c_str[u], strLen - u);
	    charCount += 1;
	  }
	  return charCount;
}

