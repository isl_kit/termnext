/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TFIDF_H_
#define TFIDF_H_

#include <vector>
#include <string.h>
#include <iostream>
#include <map>
#include <list>

#include <dirent.h>
#include "GlobalMethods.h"
#include "FilePathNode.h"


using namespace std;

struct Word2Occurrence {
    string word;
    double occurrence;
    void print(){
        cout << "word: " << word << " occurrence: " << occurrence <<  endl;
    }
};

struct TargetWord {
    string word;
    map<int,list<pair<int,int> > > docs;
    //map<int,list<pair<vector<string>,vector<string> > > > contexts; // <docid: list of occurrence<vector of prior context words, vector of afterward context words>
    double totalNrOfOccurrences;

    inline bool operator < (const TargetWord &o) const {
        return word < o.word;
    }

    inline bool operator > (const TargetWord &o) const {
        return word > o.word;
    }

    void addNewDoc(int id, int sposition, int eposition){
        // document and 1 occurrence
        list<pair <int,int> > l;
        l.push_back(pair<int,int>(sposition, eposition));
        docs[id] = l;
    }

    void addDoc(int id, int sposition, int eposition){
        //if(find(docs[id].begin(), docs[id].end(), pair<int,int>(sposition,eposition))==docs[id].end())
        docs[id].push_back(pair<int,int>(sposition,eposition));
    }

//	void increaseTotalNrOfOccurrences(int docId, int number=1){
//		docs[docId]+=number;
//	}

/*	vector<string> getContextWords(int id, int pos){
		vector<string> contextWords;

		list<pair<vector<TuplePair>,vector<TuplePair> > >::iterator it;
		for(it=contexts[id].begin(); it!=contexts[id].end(); ++it){
			if(pos<0){contextWords.push_back(it->first[-pos].key);}
			else if (pos>0){contextWords.push_back(it->second[pos].key);}
			else {contextWords.push_back(word);}
		}
		if(contextWords.size()==0) return vector<string>(NULL);
		return vector<string>(&contextWords[0], &contextWords[contextWords.size()]);
	}
	vector<string> getContextTags (int id, int pos){
		vector<string> contextTags;
		list<pair<vector<TuplePair>,vector<TuplePair> > >::iterator it;
		for(it=contexts[id].begin(); it!=contexts[id].end(); ++it){
			if(pos<0){contextTags.push_back(it->first[-pos].key);}
			else if (pos>0){contextTags.push_back(it->second[pos].key);}
		}
		if(contextTags.size()==0) return vector<string>(NULL);
		return vector<string>(&contextTags[0], &contextTags[contextTags.size()]);
	}
*/
    double getTotalNrOfOccurrences(int docId){
        return docs[docId].size();
    }

    double getTotalNrOfOccurrences(){
        double totalNrOfOccurrences = 0;
        for(map<int,list<pair<int,int> > >::iterator docsiter = docs.begin(); docsiter != docs.end(); docsiter++){
            totalNrOfOccurrences += docsiter->second.size();
        }
        return totalNrOfOccurrences;
    }

    void print(){
        cout << "Word: "<< word << "; docs: {";
        for(map<int,list<pair<int,int> > >::iterator docsiter = docs.begin(); docsiter != docs.end(); docsiter++){
            cout << docsiter ->first << " " << docsiter->second.size() ;
        }
        cout << "}" << " mutual occurrence: " << "\n";
        cout << endl;
    }
};

struct WordPair {
    TargetWord word1;
    TargetWord word2;
    double mutualOccurrence;
};

class TFIDF {
private:
	int maxNumberOfElems;
	vector<double> allDocs;
//	double totalNrOfAllWords;
	FilePathNode* flPathNode;
public:
	TFIDF(bool excludeStopWords);
	TFIDF(){};
	virtual ~TFIDF();
	void loadEnglishStopWords();
	double numberOfDocs(const char* documentDir);
	double numberOfSentences(const char* file);
	void trimWord(string& word);
	double tf(string word, double nrOfTargetWord, double mostFrequentWordInDoc);
	double tf(string word, double nrOfTargetWord);
	double augmentedTf(string word, double nrOfTargetWord, double mostFrequentWordInDoc);
	double idf(string word, double nrOfDocsWithTargetWord, double totalNrOfDocuments);
	double augmentedIdf(string word, double nrOfDocsWithTargetWord, double totalNrOfDocument);
	double tfidf(string word, vector<double> tfscores, double idfScore);
	void addWord(map<string, TargetWord>& targetWords, string word_composition, int id, int sposition, int eposition);
	bool isJunk(string& word);
//	vector<Word2Occurrence> findHighestTFIDFScores(int N);
	vector<Word2Occurrence> findHighestScore(map<string, TargetWord> targetWords, double totalNrOfDocs);
	void removeDupplicates(vector<double>& elements);
	void calculateWordOccurrenceByFile(const char* filePath, int id, map<string, TargetWord>& targetWords, map<int, double>& docId2MostFrequentWordNr, Gramm ngramm);
	void calculateWordOccurrence(iostream& stream, int id, map<string, TargetWord>& targetWords, map<int, double>& docId2MostFrequentWordNr, Gramm ngramm);
	int utfStrLength(string str);
};

#endif /* TFIDF_H_ */
