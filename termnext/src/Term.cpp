/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <set>
#include <string>
#include "Term.h"

Term::Term(int id, string value, double impScore) {
	this->id = id;
	this->value = value;
	this->importanceScore = impScore;
}

Term::Term(const Term& t)
 {
	value = t.value;
	importanceScore = t.importanceScore;
	countOfPattern = t.countOfPattern;
	cvalue = t.cvalue;
	ncvalue = t.ncvalue;
	tfidf = t.tfidf;
	posStr = t.posStr;
	explored = t.explored;
	id = t.id;
	pos_tags = t.pos_tags;
	beforeContext=t.beforeContext;
	afterContext=t.afterContext;
	algorithmVersion=t.algorithmVersion;
	databaseVersion=t.databaseVersion;
	docs.insert(t.docs.begin(), t.docs.end());

	freq = t.freq;
	docN = t.docN;
	docTotal = t.docTotal;
	freqDatabase = t.freqDatabase;
	docNDatabase = t.docNDatabase;
	docTotalDatabase = t.docTotalDatabase;
	tfscore = t.tfscore;
	idfscore = t.idfscore;
	nvalue = t.nvalue;
	a_t = t.a_t;
	a_c = t.a_c;
	wikiscore = t.wikiscore;
 };

Term::~Term() {
}

bool Term::areAllPosTagsAcceptable(vector<string> allAcceptablePos){
	for(int i=0; i<pos_tags.size(); i++){
		if(std::find(allAcceptablePos.begin(), allAcceptablePos.end(), pos_tags[i]) == allAcceptablePos.end()){
			return false;
		}
	}
	return true;
}

void Term::setpos(string tags){
	posStr = tags;
}

int Term::length(){
	if (pos_tags.size()!=0) return pos_tags.size();
	else if (posStr != ""){
		istringstream item(posStr);
		string tag;
		while (item >> tag) pos_tags.push_back(tag);
		return pos_tags.size();
	}
	else return 0;
}

double Term::calcImportanceScore(int ScoreOn){
	// ScoreOn=1: tfidf; 2:tfidf+cvalue; 3: tfidf+cvalue+nvalue
	// if a term is single word, then its important score is its tfidf;
	// if a term has multiple words, then its important score is its c-value
	if (ScoreOn&1) {
		importanceScore = tfidf;
	}
	if (ScoreOn&2) {
		importanceScore = (double)cvalue/(double)(cvalue+tfidf) + tfidf;
	}
	if (ScoreOn&4) {
		importanceScore = (double)ncvalue/(double)(ncvalue+tfidf) + tfidf;
	}
	if (ScoreOn&8) {
		importanceScore = importanceScore + wikiscore;
	}

	if (ScoreOn&16) {
		if(pos_tags.size()==1) importanceScore=importanceScore*0.259;
		else if(pos_tags.size()==2) importanceScore=importanceScore*0.421;
		else if(pos_tags.size()==3) importanceScore=importanceScore*0.171;
		else if(pos_tags.size()==4) importanceScore=importanceScore*0.069;
		else if(pos_tags.size()==5) importanceScore=importanceScore*0.037;
		else importanceScore=importanceScore*0.046;

		/*get prior prob of POS pattern*/
		//importanceScore = importanceScore * countOfPattern;
	}

	return importanceScore;
	}

void Term::print(){
	cout << "Word: "<<value<<"importScore: "<<importanceScore <<"; cvalue:"<<cvalue <<" tfidf:" << tfidf << " id:"<<id<<" docs:"<<docs.size() << " length: " << pos_tags.size() << endl;
	
}


bool Term::isBadMiddle(std::string word, std::set<string> middle){
	for (set<string>::iterator it=middle.begin(); it!=middle.end(); ++it) {
		if (word.find(*it)!=string::npos) return 1;
	}
}

bool Term::isBadStartOrEndorMiddle(string language){
/*Check the boundary words of term
 * Remove the ones starting or ending with prep
 */

if((language == "en")||(language=="EN")){
	/*initialize the set of forbidden pre- and suffix- words*/
	std::string pretmp[] = {"of","by", "on","for","with","at","to","in","as","this","that","its","those","these","but","is","are","be","the","a","an","their","our","your","from","such","no","under","between","all","any","january","february","march","april","may","june","july","august","september","october","november","december","european","his","including","other","own","paid","same","shall","some","within","full","further","giving","given","having","made","making","more","one","two","three","only","parliament","regarding","well"};
	std::string suffixtmp[] = {"of","by", "on","for","with","at","to","in","as","this","that","its","those","these","but","the","a","an","is","are","be","their","our","your","from","such","while","no","under","between","all","any","january","february","march","april","may","june","july","august","september","october","november","december","'"};
	std::string middletmp[] = {"0","1","2","3","4","5","6","7","8","9",".",";",".",":","'","+","/"};

	std::set<std::string> SetOfPre(pretmp, pretmp + sizeof(pretmp) / sizeof(pretmp[0]));
	std::set<std::string> SetOfSuffix(suffixtmp, suffixtmp + sizeof(suffixtmp) / sizeof(suffixtmp[0]));
	std::set<std::string> SetOfMIddle(middletmp, middletmp + sizeof(middletmp) / sizeof(middletmp[0]));


	string word;
	vector<string> words;
	istringstream item(value);
	while (item >> word) words.push_back(word);

	if(words.size()==0) return 1;
	else {
	// the first word
	string newStr = boost::to_lower_copy(words[0]);
	if (SetOfPre.find(newStr) != SetOfPre.end()) return 1;
	//if (newStr.find("-")==0) return 1;
	// the last word
	newStr = boost::to_lower_copy(words[words.size()-1]);
	if (SetOfSuffix.find(newStr) != SetOfSuffix.end()) return 1;
	//if (newStr.find("-")==(newStr.length()-1)) return 1;


	for(vector<string>::iterator it=words.begin(); it!=words.end(); ++it){
		if(isBadMiddle(*it, SetOfMIddle)) return 1;
		vector<string>::iterator begin=it+1;
		if(find(begin,words.end(),*it)!=words.end()) return 1; // if a word occurs more than once in a term, then the term is invalid.
	}
	return 0;
	}
}


if((language == "de")||(language=="DE")){
	/*initialize the set of forbidden pre- and suffix- words*/
	std::string pretmp[] = {"und","des","dass","auf","über","im","in","zu","von","ihres","da","wegen","als","mit","oder","vom","unter","ist","bei","zur","es","noch","wie","den","durch","er","um","an","nicht","werden","seine","einen","einer","eine","dem","sind","hat","haben","fr", "der","das","die","ber","ein", "eine","eines","so","nach","gegen","zum","gem","am","wenn","a","b","c","d","g","q","ab","aber","ob","e","gegen","muss","ben"};
	std::string suffixtmp[] = {"und","des","dass","auf","über","der","das","die","ihres","eines","einer","im","in","zu","von","da","wegen","als","mit","oder","vom","unter","ist","bei","ein","zur","es","noch","wie","den","den","durch","er","um","an","nicht","werden","seine","einen","einer","eine","dem","sind","hat","haben","aus","sein","wird","ber","so","gem","sie","i","fr","a","b","d","c","g","q","vor","nr","nach","ab","wenn","ob","wurden","zum","e","gegen","muss","ben"};
	std::string middletmp[] = {"0","1","2","3","4","5","6","7","8","9",".",";",".",":","'",",","(",")","i","+","/","[","]"};

	std::set<std::string> SetOfPre(pretmp, pretmp + sizeof(pretmp) / sizeof(pretmp[0]));
	std::set<std::string> SetOfSuffix(suffixtmp, suffixtmp + sizeof(suffixtmp) / sizeof(suffixtmp[0]));
	std::set<std::string> SetOfMIddle(middletmp, middletmp + sizeof(middletmp) / sizeof(middletmp[0]));


	string word;
	vector<string> words;
	istringstream item(value);
	while (item >> word) words.push_back(word);

	if(words.size()==0) return 1;
	else {
	// the first word
	string newStr = boost::to_lower_copy(words[0]);
	if (SetOfPre.find(newStr) != SetOfPre.end()) return 1;
	if (newStr.find("-")==0) return 1;

	// the last word
	newStr = boost::to_lower_copy(words[words.size()-1]);
	if (SetOfSuffix.find(newStr) != SetOfSuffix.end()) return 1;
	if (newStr.find("-")==(newStr.length()-1)) return 1;

	for(vector<string>::iterator it=words.begin(); it!=words.end(); ++it){
		if(isBadMiddle(*it, SetOfMIddle)) return 1;
		vector<string>::iterator begin=it+1;
		if(find(begin,words.end(),*it)!=words.end()) return 1; // if a word occurs more than once in a term, then the term is invalid.
	}
	return 0;
	}
}



return 0;
}

void Term::calculateNCValue(map<string, double> NCWeightsBefore, map<string, double> NCWeightsAfter){
	double score=0;
	for(int i=0; i<beforeContext.size(); i++) score+=NCWeightsBefore[beforeContext[i].key];
	for(int i=0; i<afterContext.size(); i++) score+=NCWeightsAfter[afterContext[i].key];
	ncvalue = 0.8*cvalue + 0.2*score;
	nvalue = score;
}

std::string Term::removeSubstring( std::string str, string sub )
{
	string s = str;
	string::size_type foundpos = s.find(sub);
	while (foundpos != string::npos){
		s.erase(s.begin()+foundpos, s.begin()+foundpos+sub.length());
		foundpos = s.find(sub);
	}
	return s ;
}

std::string Term::removeUmlaut(string str){

	string s = str;
	s=removeSubstring(s,"ä");
	s=removeSubstring(s,"ö");
	s=removeSubstring(s,"ü");
	s=removeSubstring(s,"Ä");
	s=removeSubstring(s,"Ö");
	s=removeSubstring(s,"Ü");
	s=removeSubstring(s,"ß");

	return s;
}

string Term::getInfo(){
	std::ostringstream ss;
	ss << a_t <<" ;\t"<<  a_c <<" ;\t"<< freq <<" ;\t" << docN <<" ;\t"<< docTotal <<" ;\t"<< freqDatabase<<" ;\t"<< docNDatabase<<" ;\t"<< docTotalDatabase <<" ;\t"<< tfscore<<" ;\t"<< idfscore <<" ;\t"<< tfidf <<" ;\t"<< nvalue <<" ;\t"<< cvalue <<" ;\t"<<  ncvalue <<" ;\t"<< wikiscore;

	string s(ss.str());
	return s;
}



