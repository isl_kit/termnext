/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERM_H_
#define TERM_H_
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <boost/algorithm/string.hpp>
#include "lapos-0.1.2/postagger.h"

using namespace std;
using namespace boost;

struct TID2Importance{
	int id;
	double importance;
};

class Term {
public:
	string value;
	double importanceScore;
	double countOfPattern;
	double cvalue;
	double ncvalue;
	double tfidf;
	double wikiscore; // 0 or 1
	int id;
	map< int, std::list<pair<int,int> > > docs; // <docid, <beginPos, endPos>>: The term occurs in which doc and the positions in the doc.
	string posStr; // POS_tags string: concatenate from vector<string> pos_tags.
	vector<string> pos_tags;
	vector<TuplePair> beforeContext; //context words before the term
	vector<TuplePair> afterContext; // context words after the term
	bool explored;
	double algorithmVersion;		// show version information for database and algorithm
	double databaseVersion;

	inline bool operator < (const Term &o) const { // compare the number of words in a term
		return pos_tags.size() > o.pos_tags.size();
	}

	inline bool operator > (const Term &o) const { // compare the number of words in a term
		return pos_tags.size() < o.pos_tags.size();
	}

	inline bool operator == (const Term &o) const{
		return value == o.value;
	}

	Term (){importanceScore=0; countOfPattern=0; cvalue=0; ncvalue=0;tfidf=0; id=0;wikiscore=0;
	freq=0; docN=0;docTotal=0;freqDatabase=0;docNDatabase=0;docTotalDatabase=0;tfscore=0;idfscore=0;nvalue=0;a_t=0;a_c=0;
	};
	Term(int id, string value, double impScore);
	Term(const Term& t);
	virtual ~Term();
	bool areAllPosTagsAcceptable(vector<string> allAcceptablePos);
	void setpos(string tags);
	int length();
	void print();
	double calcImportanceScore(int ScoreOn);
	bool isBadStartOrEndorMiddle(string language);
	bool isBadMiddle(string word, set<string> middle);
	void calculateNCValue(map<string, double> NCWeightsBefore, map<string, double> NCWeightsAfter);
	std::string removeSubstring( std::string str, string sub );
	std::string removeUmlaut(string str);

	string getInfo();

	double freq;	//term frequency in input files
	double docN;	//how many docs of the input files the term occurs
	double docTotal;//total number of input docs

	double freqDatabase; // term frequency in background data set.
	double docNDatabase; //how many docs of the background data set the term occurs
	double docTotalDatabase; // total doc number of the background data set
	double tfscore;
	double idfscore;
	double nvalue;
	double a_t, a_c; //used for calculating Cvalue


};

#endif /* TERM_H_ */
