/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Validator.h"
#include "GlobalMethods.h"

#define MAX_TERM_SPECIFICITY 10

Validator::Validator() {
}

Validator::~Validator() {
}

bool Validator::isTermSpecificityValid(int termSpecificity){
	if(termSpecificity < 1){
		return false;
	}
	if(termSpecificity > MAX_TERM_SPECIFICITY){
		return false;
	}
	return true;
}

bool Validator::isNGrammValueValid(int ngammCode){
	if(ngammCode < 1){
			return false;
		}
		if(ngammCode > MAX_NGRAM){
			return false;
		}
		return true;
}


bool Validator::isLimitedNrOfElementsValid(int limitedNrOfElements){
	if(limitedNrOfElements < 1){
			return false;
		}
		return true;
}
