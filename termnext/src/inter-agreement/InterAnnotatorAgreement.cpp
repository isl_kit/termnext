/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "InterAnnotatorAgreement.h"
#include <iostream>
#include <fstream>
#include <list>
#include <boost/algorithm/string.hpp>

using namespace boost::algorithm;

InterAnnotatorAgreement::InterAnnotatorAgreement() {

}

InterAnnotatorAgreement::~InterAnnotatorAgreement() {
}

double mutualAgreement = 0;
double mutualDisagreement = 27789;
double agreedA = 0;
double disAgreedA = 0;
double agreedB = 0;
double disAgreedB = 0;

list<Term> InterAnnotatorAgreement::readTerminologyList(const char* filepath){
	list<Term> termlist;

	ifstream instream;
	instream.open(filepath);

	string line;
	int termId = 1;
	while(!instream.eof()){
		getline(instream, line);
		termlist.push_back(Term(termId++, line, 0.0));
	}

	instream.close();

	return termlist;
}


void InterAnnotatorAgreement::printTerminologyList(list<Term> termlist){
	  for(list<Term>::iterator termlistiter = termlist.begin(); termlistiter != termlist.end(); termlistiter++){
		  string elem = (*termlistiter).value;
		  cout << elem << endl;
	  }
}

void InterAnnotatorAgreement::printStatistics(){
	cout << "mutualAgreement: " << mutualAgreement << endl;
	cout << "mutualDisagreement: " << mutualDisagreement << endl;
	cout << "agreedA: " << agreedA << endl;
	cout << "agreedB: " << agreedB << endl;
	cout << "disAgreedA: " << disAgreedA << endl;
	cout << "disAgreedB: " << disAgreedB << endl;
}

void InterAnnotatorAgreement::countMatches(list<Term> annotator1, list<Term> annotator2){
	cout << annotator1.size() << " " << annotator2.size() << endl;

	for(list<Term>::iterator annotator1iter = annotator1.begin(); annotator1iter != annotator1.end(); annotator1iter++){
			  Term& term1 = (*annotator1iter);
			  string term1str =term1.value;
			  trim(term1str);
			  vector<string> term1strArr;
			  split(term1strArr, term1str, boost::is_any_of("[ |\t]"));
			  for(list<Term>::iterator annotator2iter = annotator2.begin(); (annotator2iter != annotator2.end()); annotator2iter++){
				  Term& term2 = (*annotator2iter);
				  string term2str =term2.value;
				  trim(term2str);
				  vector<string> term2strArr;
				  split(term2strArr, term2str, boost::is_any_of("[ |\t]"));
				  if(term1str.compare(term2str.c_str()) == 0){
					  term1.importanceScore = term2strArr.size();
					  term2.importanceScore = term2strArr.size();
					  term2.explored = true;
				  }else if (term1str.find(term2str) != string::npos){
					  if(term2.importanceScore < term2strArr.size()){
						  term2.importanceScore = term2strArr.size();
					  }
					  if(term1.importanceScore < term2strArr.size()){
						  term1.importanceScore = term2strArr.size();
					  }
					  term2.explored = true;
				  }else if(term2str.find(term1str) != string::npos ){
					  if(term2.importanceScore < term1strArr.size()){
					  	  term2.importanceScore = term1strArr.size();
					  }
					  if(term1.importanceScore < term1strArr.size()){
					      term1.importanceScore = term1strArr.size();
					  }
					  term2.explored = true;
				  }
			  }
//			  if(a1Agreed != 0){
//				  if(a1Agreed > a2Agreed){
//					  //double proportionAgreed = (double)(((double)a2Agreed)/((double)a1Agreed));
//					//  mutualAgreement += a2Agreed;
//					  term1.importanceScore = a2Agreed;
//					  term2.importanceScore = a2Agreed;
//					  agreedA += (a1Agreed-a2Agreed);
//					  disAgreedB += a1Agreed-a2Agreed;
//				  }else{
//					  //double proportionAgreed = (double)(((double)a1Agreed)/((double)a2Agreed));
//					  mutualAgreement += a1Agreed;
//					  agreedB += a2Agreed-a1Agreed;
//					  disAgreedA += a2Agreed-a1Agreed;
//				  }
//			  }else{
//				  	 agreedA += term1strArr.size();
//				  	 disAgreedB += term1strArr.size();
//			  }
//
			  term1.explored = true;
		  }
	for(list<Term>::iterator annotator1iter = annotator1.begin(); annotator1iter != annotator1.end(); annotator1iter++){
		  		 Term term1 = (*annotator1iter);
		  		 vector<string> term1strArr;
		  		 trim(term1.value);
		  		 split(term1strArr, term1.value, boost::is_any_of("[ |\t]"));
		  		 if(term1.explored){
		  			 mutualAgreement += term1.importanceScore;
		  			 disAgreedB += term1strArr.size() - term1.importanceScore;
		  			 agreedA += term1strArr.size() - term1.importanceScore;
		  		 }else{
		  			 agreedA += term1strArr.size();
		  			 disAgreedB += term1strArr.size();
		  		 }
		  }

	  for(list<Term>::iterator annotator2iter = annotator2.begin(); annotator2iter != annotator2.end(); annotator2iter++){
		 Term term2 = (*annotator2iter);
		 vector<string> term2strArr;
		 trim(term2.value);
		 split(term2strArr, term2.value, boost::is_any_of("[ |\t]"));
		 if(term2.explored){
			// mutualAgreement += term2.importanceScore;
			 disAgreedA += term2strArr.size() - term2.importanceScore;
			 agreedB += term2strArr.size() - term2.importanceScore;
		 }else{
			 agreedB += term2strArr.size();
			 disAgreedA += term2strArr.size();
		 }
      }

}

double InterAnnotatorAgreement::kappa(){
	double all = mutualAgreement + mutualDisagreement + agreedA + agreedB;
	double p_a = (double)(mutualAgreement + mutualDisagreement)/((double)all);
	double a_yes =  (double)(mutualAgreement + agreedA)/all;
	double b_yes =  (double)(mutualAgreement + agreedB)/all;
	double a_no =  (double)(mutualDisagreement + disAgreedA)/all;
	double b_no =  (double)(mutualDisagreement + disAgreedB)/all;
	double p_e =a_yes*b_yes + a_no*b_no;

	cout << "p_a: " << p_a << endl;
	cout << "p_e: " << p_e << endl;
	return (p_a - p_e)/(1-p_e);
}

int maininterannotagree(int argc, char **argv) {
	InterAnnotatorAgreement interAnnotAgreement;
	list<Term> annotator1 = interAnnotAgreement.readTerminologyList("/home/nkokhlik/projects/eu-bridge/EP/terminology/annotation/parallel-annotation/Term-Extraction-Manual-By-MB.txt.clean2");
	list<Term> annotator2 = interAnnotAgreement.readTerminologyList("/home/nkokhlik/projects/eu-bridge/EP/terminology/annotation/parallel-annotation/Terminology-extraction-manual-MFextracted-terminology-MARCIN.txt");

//	interAnnotAgreement.printTerminologyList(annotator1);
//	interAnnotAgreement.printTerminologyList(annotator2);
	interAnnotAgreement.countMatches(annotator1, annotator2);
	interAnnotAgreement.printStatistics();
	double kappa = interAnnotAgreement.kappa();
	cout << kappa << endl;
}

