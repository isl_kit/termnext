/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef INTERANNOTATORAGREEMENT_H_
#define INTERANNOTATORAGREEMENT_H_

#include <list>
#include <string>
#include "../Term.h"

using namespace std;

class InterAnnotatorAgreement {
public:
	InterAnnotatorAgreement();
	virtual ~InterAnnotatorAgreement();
	list<Term> readTerminologyList(const char* filepath);
	void countMatches(list<Term> annotator1, list<Term> annotator2);
	double kappa();
	void printTerminologyList(list<Term> termlist);
	void printStatistics();
};

#endif /* INTERANNOTATORAGREEMENT_H_ */
