/*
 * postagger.h
 *
 *  Created on: Oct 8, 2013
 *      Author: nkokhlik
 */

#ifndef POSTAGGER_H_
#define POSTAGGER_H_

#include <stdio.h>
#include <fstream>
#include <map>
#include <list>
#include <set>
#include <iomanip>
#include <iostream>
#include <cfloat>
#include <sstream>
//#include "maxent.h"
#include "crf.h"
#include "common.h"
#include <sys/time.h>

using namespace std;

struct TuplePair{
	string key;
	string value;
};

class postagger{
public:
   postagger();
   ~postagger();
   void loadModel(string modelPath);
   vector<TuplePair> tag(string text);
};

#endif /* POSTAGGER_H_ */
