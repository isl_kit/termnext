/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "JavaConnector.h"
#include <stdio.h>
#include <jni.h>
#include <iostream>
#include <string.h>
#include <sstream>
#include <thread>


JavaVM *jvm;
JNIEnv *env;
jobject neClassifier;
jmethodID xmlClassifierDesc;

JavaConnector::JavaConnector() {
}

JavaConnector::~JavaConnector() {
}


void JavaConnector::create(const char* path2NERTagger, const char* path2model) {
    cout << "... Starting to create jvm ... " << endl;

    JavaVMInitArgs vm_args;
    JavaVMOption* options = new JavaVMOption[1];
    string path2NERTaggerStr = "-Djava.class.path="+string(path2NERTagger);
//    options[0].optionString = path2NERTaggerStr.c_str();
    options[0].optionString = new char [path2NERTaggerStr.length()];
    path2NERTaggerStr.copy(options[0].optionString,string::npos);
    options[0].optionString[path2NERTaggerStr.length()] = 0;
    vm_args.version = JNI_VERSION_1_6;
    vm_args.nOptions = 1;
    vm_args.options = options;
    vm_args.ignoreUnrecognized = false;
    /* load and initialize a Java VM, return a JNI interface
     * pointer in env */

    if (jvm == NULL)
    {
    	//ScopedLock lock(mJvmLock);
    	if (jvm == NULL)
    	{
    		//ScopedLock lock(mJvmLock);
    		if (jvm == NULL)
    		{
    			int ret =  JNI_CreateJavaVM(&jvm, (void**)&env, &vm_args);

    			if(ret < 0)  cout << "Create JVM failed!" << endl;
    			else cout << "Create JVM successful!" << endl;
    		}
    		else {
    			cout << "jvm Not NULL" << endl;
    			jvm->AttachCurrentThread((void **)&env, NULL);
    		}
    	}
    	else {
			cout << "jvm Not NULL" << endl;
			jvm->AttachCurrentThread((void **)&env, NULL);
    	}
    }






    delete options;
    /* invoke the Main.test method using the JNI */
    jclass cls = env->FindClass("edu/stanford/nlp/ie/crf/CRFClassifier");
    jmethodID mid = env->GetStaticMethodID(cls, "getClassifier", "(Ljava/lang/String;)Ledu/stanford/nlp/ie/crf/CRFClassifier;");
    jstring modelName = (jstring)(env->NewStringUTF(path2model));
    neClassifier = env->CallStaticObjectMethod(cls, mid, modelName);
    xmlClassifierDesc = env->GetMethodID(cls, "classify", "(Ljava/lang/String;)Ljava/util/List;");
    cout << "... Finishing to create jvm ... " << endl;
}

void JavaConnector::destroy(){
    /* We are done. */
    jvm->DestroyJavaVM();
}


map<pair<string, pair<int,int>>, string> JavaConnector::extractNamedEntities(const char* text){
	map<pair<string, pair<int,int>>, string> neKeyValueMap;


	if(jvm != NULL) {
		jvm->AttachCurrentThread((void **)&env, NULL);
	}


	jstring text2Tag = (jstring)(env->NewStringUTF(text));

    jobject list2_corelabel = env->CallObjectMethod(neClassifier, xmlClassifierDesc, text2Tag);

    jobject list2_corelabel_iter = getIterator(env, list2_corelabel);

    //get the signature from the package
     jclass iterator_class = env->GetObjectClass(list2_corelabel_iter);
     jmethodID methodId_for_hasNext = env->GetMethodID(iterator_class, "hasNext",
             "()Z");
     jmethodID methodId_for_next = env->GetMethodID(iterator_class, "next",
             "()Ljava/lang/Object;");
     jmethodID methodId_for_orig_text = env->GetMethodID(env->FindClass("edu/stanford/nlp/ling/CoreLabel"), "originalText", "()Ljava/lang/String;");
     jmethodID methodId_for_tag = env->GetMethodID(env->FindClass("edu/stanford/nlp/ling/CoreLabel"), "get", "(Ljava/lang/Class;)Ljava/lang/Object;");
     jmethodID methodId_for_bposition = env->GetMethodID(env->FindClass("edu/stanford/nlp/ling/CoreLabel"), "beginPosition", "()I");
     jmethodID methodId_for_eposition = env->GetMethodID(env->FindClass("edu/stanford/nlp/ling/CoreLabel"), "endPosition", "()I");

     while (env->CallBooleanMethod(list2_corelabel_iter, methodId_for_hasNext)) {
       jobject list1_corelabel = env->CallObjectMethod(list2_corelabel_iter,
                   methodId_for_next);
       jobject list1_corelabel_iter = getIterator(env, list1_corelabel);
       const char* prevTag = "O";
       string prevPhrase = "";
       int bposition = 0;
       int eposition = 0;
       while(env->CallBooleanMethod(list1_corelabel_iter, methodId_for_hasNext)){
    	   jobject corelabel = env->CallObjectMethod(list1_corelabel_iter,
    	                      methodId_for_next);
    	   jobject origtextobj = env->CallObjectMethod(corelabel, methodId_for_orig_text);
    	   jclass answannotcl = env->FindClass("edu/stanford/nlp/ling/CoreAnnotations$AnswerAnnotation");
    	   jobject tagobj = env->CallObjectMethod(corelabel, methodId_for_tag, answannotcl);
    	   jint bpositionobj = env->CallIntMethod(corelabel, methodId_for_bposition);
    	   jint epositionobj = env->CallIntMethod(corelabel, methodId_for_eposition);
    	   const char* origtextobjStr = (const char*)env->GetStringUTFChars((jstring)origtextobj, JNI_FALSE);
    	   const char* tag = (const char*)env->GetStringUTFChars((jstring)tagobj, JNI_FALSE);

    		if(strcasecmp(tag, prevTag) != 0){
    		   if(strcasecmp(prevTag, "O") != 0){
    				   char key[prevPhrase.size()];
    				   char value[strlen(prevTag)];
    				   strcpy(key, prevPhrase.c_str());
    				   strcpy(value, prevTag);
    				   pair<string,pair<int,int>> p = pair<string,pair<int,int>>((string)key, pair<int,int>(bposition, eposition));
    				   neKeyValueMap[p] = ((string)value);
    			 }
    				prevPhrase = "";
    				bposition = (int)bpositionobj;
    				eposition = (int)epositionobj;
    		 }
    		 if( strcasecmp(tag, "O") != 0){
    			   if(!prevPhrase.empty()){
    				   prevPhrase += " ";
    			   }
    			   prevPhrase += origtextobjStr;
    		 }
    	   prevTag = tag;
       	   }
     }
/*     if (this_id != main_id) {
    	 jvm->DetachCurrentThread();
     }
     */
    return neKeyValueMap;
}

jobject JavaConnector::getIterator(JNIEnv* env, jobject list_package_object) {

   //make a java call to get List.Iterator()
   jclass list_class = env->GetObjectClass(list_package_object);
   jmethodID methodId = env->GetMethodID(list_class, "iterator",
               "()Ljava/util/Iterator;");
   jobject iterator = env->CallObjectMethod(list_package_object, methodId);
   return iterator;
}
