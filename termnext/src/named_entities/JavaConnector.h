/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef JAVACONNECTOR_H_
#define JAVACONNECTOR_H_

#include <jni.h>
#include <map>
#include <string>

using namespace std;

/*
 * Call NE tagger written in Java.
 *
 */

class JavaConnector {
public:
	JavaConnector();
	virtual ~JavaConnector();
	void create(const char* path2NERTagger, const char* path2model);
	void destroy();
	map<pair<string, pair<int,int> >, string> extractNamedEntities(const char* text);
	jobject getIterator(JNIEnv* env, jobject list_package_object);

};

#endif /* JAVACONNECTOR_H_ */
