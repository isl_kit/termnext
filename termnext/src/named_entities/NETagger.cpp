/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "NETagger.h"
#include "JavaConnector.h"
#include <fstream>
#include <ctype.h>
#include <algorithm>

//JavaConnector jConnector;

NETagger::NETagger(const char* path2NERTagger, const char* path2model) {
	jConnector.create(path2NERTagger, path2model);
}

NETagger::~NETagger() {
	jConnector.destroy();
}

void NETagger::extractNamedEntities(vector<Phrase>& phrases, const char* text, Types types){

	// neMap: extracted Named Entities: type, NE, begin_positions, end_positions
	map<pair<string, pair<int,int> >, string> neMap = jConnector.extractNamedEntities(text);
	map<pair<string, pair<int,int>>, string>::iterator neMapIter = neMap.begin();


	for(; neMapIter != neMap.end(); neMapIter++){
		Phrase phrase;
		Type t_ = types.getTypeByName( neMapIter->second);
		phrase.t = t_;
		phrase.cursorBegPos =neMapIter->first.second.first;
		phrase.cursorEndPos =neMapIter->first.second.second;
		string val = neMapIter->first.first;

		vector<string> valArr;
		boost::split(valArr, val, boost::is_any_of(","));
		int i = 0;
		int bpos = phrase.cursorBegPos;
		while(i<valArr.size()){
			phrase.value = valArr[i];
			trim(phrase.value);
			string tempVal =  valArr[i];
			while(tempVal.size() > 0 && tempVal[0] == ' '){
				tempVal = tempVal.substr(1, tempVal.size());
				bpos++;
			}
			phrase.cursorBegPos = bpos;
			phrase.cursorEndPos = bpos +phrase.value.size();

			while (isalpha(text[phrase.cursorEndPos])||isdigit(text[phrase.cursorEndPos])) phrase.cursorEndPos++;


			if(phrase.value!=""){
				string tmpstr = phrase.value;
				// filterd out high-freq "organization" in stopwords list
				std::transform(tmpstr.begin(), tmpstr.end(), tmpstr.begin(), ::tolower);
				if (NEStopList.find(tmpstr)==NEStopList.end()) {phrases.push_back(phrase);}
				else if (phrase.t.name != "org") {phrases.push_back(phrase);}
			}

			bpos +=  phrase.value.size() +1;
			i++;
		}
	}
}

void NETagger::printNamedEntities(vector<Phrase>& phrases){
	cout << "All detected Named-Entities..." << endl;
	for(int i=0; i<phrases.size(); i++){
		string phrase = phrases[i].value;
		Type t = phrases[i].t;
		cout << phrase <<  " " <<  t.label << " "  << phrases[i].cursorBegPos << ":" << phrases[i].cursorEndPos << endl ;
	}
}

void NETagger::loadNEStopList(){
	string filename = "../../resource/NE.blacelist.v1";
	ifstream in;
	in.open(filename.c_str());
	if (in) {cout << "Name Entity Stop Word List is loaded!" << endl;}
	else {cout << "Name Entity Stop Word List is missing!" << endl;}
	string line;
	while(getline(in, line) != NULL){
		NEStopList.insert(line);
	}
	in.close();
}
