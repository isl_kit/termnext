/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DOCUMENT_H_
#define DOCUMENT_H_

#include "Types.h"
#include "../GlobalMethods.h"
#include <vector>
#include <map>
#include <set>
#include <fstream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include "JavaConnector.h"

using namespace std;
using namespace boost;

struct Phrase{
	string value;
	Type t;
	int cursorBegPos;
	int cursorEndPos;
};

class NETagger {
public:
	NETagger(const char* path2NERTagger, const char* path2model);
	virtual ~NETagger();
	void extractNamedEntitiesFromArchive(vector<Phrase>& phrases, const char* path, Types types);
	void extractNamedEntities(vector<Phrase>& phrases, const char* text, Types types);
	void printNamedEntities(vector<Phrase>& phrases);
	void loadNEStopList();
private:
	JavaConnector jConnector;
	set<string> NEStopList;

};

#endif /* DOCUMENT_H_ */
