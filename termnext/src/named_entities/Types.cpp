/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Types.h"


//string config_file = "config/namedentities.conf";

Types::Types() {
}

Types::~Types() {
}

Type Types::getTypeByName(string name){
	vector<Type> allTypes = list();
	if(allTypes.empty()){
		throw "No Type found";
	}

	for(int i=0; i<allTypes.size(); i++){
		Type t = allTypes[i];
		boost::to_lower(name);
		if(t.name.compare(name) == 0){
			return t;
		}
	}
	Type t;
	t.id=-1;
	t.color="#FFFFFF";
	t.label="NONE";
	t.name="Unknown";
	return t;
}

vector<Type> Types::list(){
	return allTypes;
}
