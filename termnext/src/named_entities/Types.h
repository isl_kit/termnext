/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TYPES_H_
#define TYPES_H_

#include <iostream>
#include <libconfig.h++>
#include <string.h>
#include <vector>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace libconfig;

struct	Type{
	int id;
	string name;
	string label;
	string color;
};

class Types {


public:
	vector<Type> allTypes;
	Types();
	virtual ~Types();
	vector<Type> list();
	Type getTypeByName(string name);
};

#endif /* TYPES_H_ */
