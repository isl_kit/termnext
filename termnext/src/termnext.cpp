/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <Python.h>
#include <iostream>
#include <map>
#include <algorithm>

#include "termnext.h"
#include "ExtractCountries.h"
#include "named_entities/NETagger.h"
#include "named_entities/Types.h"
#include "ConfigLoader.h"
#include "SystemLoader.h"

using namespace std; 

/*Version=0   Cvalue algorithm + background database*/
/*Version=1   + NCvalue algorithm + deal with utf-8 input characters + check term valid with internal characters + json format generator*/
/*Version=2   + more filtering on the term candidates list: reomove the shorter ones if there are similar to each other*/
/*Version=4   + priProb(term-length); + priProb(term-pattern) */
int Version = 4;
int dataBaseVersion = 4; // 0 ==> 050614(date) 1 ==> 070214(date: 399 EP document) 2==> 1+europal (+800 docs) 3==>2+17000EP docs
						 // 4==>2+27188 EP docs

ConfigLoader confLoader;
Validator validator;
DocumentFilter df;
vector<char*> allCountries;

Termnext::Termnext(){
}

Termnext::~Termnext(){
}


void Termnext::configureNGramm(Gramm ngramm){
	this->ngramm = ngramm;
}

pair<int, int> Termnext::getVersions(){
	return make_pair(Version, dataBaseVersion);
}

void Termnext::pushData(stringstream& data, int counter, string language){
	/* do POS tagging on the input data */
	df.postagging(data, language);

	/*calculate the occurence of ngrams*/
	df.calculateWordOccurrence(data, counter, ngramm);

}

void Termnext::pushSOD(){

	df.reset();
}

Validator Termnext::getValidator(){
	return validator;
}

NETagger* Termnext::getNETagger(const char* path2NERTagger, const char* path2model, Types types){
	NETagger* neTagger = new NETagger(path2NERTagger, path2model);
	neTagger->loadNEStopList();
	return neTagger;
}


bool termCompare(TID2Importance a, TID2Importance b){
	return (a.importance > b.importance);
}




vector<Term> Termnext::extractTerminologyListFast(const int TermMaxLength, int termSpecificity, long limitNrOfTerms, int docCounter, string language){

	//Step1 : pos pattern filtering
	cerr << "Start term Filtering ..." << endl;
	vector<Term> termCandidates = df.filterByPOSPatterns (TermMaxLength, language);

	//Step2: TFIDF on candidate terms+CValue
	cerr << "Start score calculation ..." << endl;
	pair< map<int, Term>, vector<TID2Importance> > termListPair = df.calculateValue(termCandidates, termSpecificity, TermMaxLength, docCounter);

	map<int, Term> termMap = termListPair.first;
	vector<TID2Importance> tid2importvect = termListPair.second;
	vector<Term> termList;
	std::sort(tid2importvect.begin(), tid2importvect.end(), termCompare);
	for(int i=0; i<tid2importvect.size(); i++){
		int id = tid2importvect[i].id;
		Term t = termMap.at(id);
		t.algorithmVersion = Version;
		t.databaseVersion = dataBaseVersion;

		if(t.docs.size()>0) {
			termList.push_back(t); }
	}

	if(termList.size()==0) return vector<Term>(0);
	if(limitNrOfTerms < 0 || limitNrOfTerms >= termList.size()){
			return termList;
	}
	return vector<Term>(&termList[0], &termList[limitNrOfTerms]);
}




vector<Term> Termnext::sortTerminologyByImportance(vector<Term> termList){
	if(termList.size() == 0){
		return termList;
	}
	return mergesortTerminologyByImportance(termList, 0, termList.size()-1);
}

vector<Term> Termnext::mergesortTerminologyByImportance(vector<Term> termList, int start, int end){
	// base case
	if(end - start < 1){
		vector<Term> tList;
		tList.push_back(termList[end]);
		return tList;
	}

	int middle = start + (end - start)/2;
	// left branch
	vector<Term> leftTerms = mergesortTerminologyByImportance(termList, start, middle);
	// right branch
	vector<Term> rightTerms = mergesortTerminologyByImportance(termList, middle+1, end);

	vector<Term> mergedTerms;
	mergeTerminologyByImportance(mergedTerms, leftTerms, rightTerms);
	return mergedTerms;
}

void Termnext::mergeTerminologyByImportance(vector<Term>& mergedArr, vector<Term> leftTerms, vector<Term> rightTerms){
	int i=0;
	int j=0;

	while (i<leftTerms.size() && j<rightTerms.size()){
		if(leftTerms[i].importanceScore > rightTerms[j].importanceScore){
			mergedArr.push_back(leftTerms[i++]);
		}else{
			mergedArr.push_back(rightTerms[j++]);
		}
	}

	while(i < leftTerms.size()){
		mergedArr.push_back(leftTerms[i++]);
	}

	while(j < rightTerms.size()) {
		mergedArr.push_back(rightTerms[j++]);
	}
}

void Termnext::initTermExtractor(){
	df.init(supportedPosTypes, allCountries, true);
}

Types Termnext::getTypes(){
	return this->types;
}

vector<string> Termnext::getPOSTypes(){
	return supportedPosTypes;
}

void Termnext::setPosTagger(postagger pt,string language){
	df.postagger_ = &pt;
}

void Termnext::setTypes(Types ts){
	this->types = ts;
}

void Termnext::setPOSTypes(vector<string> supportedPosTypes){
	this->supportedPosTypes = supportedPosTypes;
	df.init(supportedPosTypes, allCountries, true);
}

int Termnext::setPosPatterns(string language){
	string filename;
	if ((language == "en")||(language=="EN")) {
		filename = "../resource/extract_pattern_clean.freq2.txt";
	}
	else {
		filename = "";
	}

	df.loadPosPatterns(filename, language);
}

int Termnext::setTermBlackLists(string language){
	//string filename =  "/home/yzhang/src/eu-bridge/EU-Bridge/resource/blackList.all";
	string filename;
	 if((language=="en")||(language=="EN")) {filename = "../resource/blackList.en.v6.freq100";}
	 if((language=="de")||(language=="DE")) filename = "../resource/blackList.de.v3";
	df.loadTermBlackList(filename);
}

int Termnext::setNEBlackLists(string language){
	//string filename =  "/home/yzhang/src/eu-bridge/EU-Bridge/resource/blackList.all";
	string filename;
	if((language=="en")||(language=="EN")) filename = "../resource/NE.blacelist.v1";
	df.loadNEStopWords(filename);
}

int Termnext::setContextWeights(string language){
if((language=="en")||(language=="EN")){
	string filenameBefore =  "../NCValueContextWeights/all.txt.Win2.before";
	string filenameAfter =  "../NCValueContextWeights/all.txt.Win2.after";
	df.loadNCWeights(filenameBefore, filenameAfter);
}

}

int Termnext::setWikiList (string language){
	string filename;
	if((language=="en")||(language=="EN")){
		filename = "../resource/wikiTitles.filtered.0-0.en.lc";
		df.loadWiKi(filename,"en");
	}
}

int Termnext::setBackgroundData(string language){
	// load data from the background database;
	string dir="../resource/";

	if ((language == "en")||(language=="EN")){
//		df.loadWordOccurrenceFromDatabase(dir+"database070214.en.ngrams");
//		df.loadDocsTotalCountFromDatabase(dir+"database070214.en.docs");
		df.loadWordOccurrenceFromDatabase(dir+"databaseDec2014.en.ngrams");
		df.loadDocsTotalCountFromDatabase(dir+"databaseDec2014.en.docs");
//		df.addWordOccurrenceFromDatabase(dir+"databaseNov14.en.ngrams");
//		df.addDocsTotalCountFromDatabase(dir+"databaseNov14.en.docs");
		df.addWordOccurrenceFromDatabase(dir+"europal.en.ngrams");
		df.addDocsTotalCountFromDatabase(dir+"europal.en.docs");

	}

	if ((language == "de")||(language=="DE")){
		df.loadWordOccurrenceFromDatabase(dir+"database070214.de.noUmlaut.ngrams");
		df.loadDocsTotalCountFromDatabase(dir+"database070214.de.docs");
		df.addWordOccurrenceFromDatabase(dir+"europal.de.noUmlaut.ngrams");
		df.addDocsTotalCountFromDatabase(dir+"europal.de.docs");
	}
}

int main(){
  /*
	SystemLoader sysLoader;
	sysLoader.init("../src/config/eu-bridge.conf");
	sysLoader.initpostagger("../model/pos/model_wsj00-18/");

	string language="en";

	termnext eubridge;
	eubridge.setPOSTypes(sysLoader.supportedPosTypes);
	eubridge.setTypes(sysLoader.types);
	eubridge.setPosTagger(sysLoader.ptagger,language);


	cout << "Starting terminology extraction ... " << endl;
    int docCounter = 0;

    eubridge.configureNGramm(Bi);
    eubridge.pushSOD();

	vector<Term> termList = eubridge.extractTerminologyList(false, 5, 1000, docCounter, language);
	vector<Term>::iterator termListIter = termList.begin();
	while(termListIter != termList.end()){
		cout << termListIter->value << " " << termListIter->importanceScore;
		map<int, list<pair<int,int> > > docs = termListIter->docs;
		for(map<int, list<pair<int,int>> >::iterator docsiter = docs.begin(); docsiter != docs.end(); docsiter++){
			int docId = docsiter->first;
			list<pair<int,int>> positions = docsiter->second;
			for(list<pair<int,int>>::iterator positionsiter = positions.begin(); positionsiter != positions.end(); positionsiter++){
				int sposition = positionsiter->first;
				int eposition = positionsiter->second;
			}
		}
		termListIter++;
	}
	cout << "Finishing terminology extraction ... " << endl;

	return 0;
  */
}
