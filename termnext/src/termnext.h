/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef EUBRIDGE_H_
#define EUBRIDGE_H_

#include "named_entities/NETagger.h"
#include "DocumentFilter.h"
#include "Term.h"
#include "Validator.h"


class Termnext {

public:
		vector<string> supportedPosTypes;
		Types types;
		Gramm ngramm;
		Termnext();
		virtual ~Termnext();
		void initTermExtractor();
		void configureNGramm(Gramm ngramm);
		void pushData(stringstream& data, int counter, string language);
		void pushSOD();
		NETagger* getNETagger(const char* path2NERTagger, const char* path2model, Types types);
		vector<Term> extractTerminologyList(const char* model, const char* path2Resources, vector<string> posTypes, Gramm ngramm, bool includeLinguisticFeatures, int termSpecificity, long limitNrOfTerms, int docCounter);
		vector<Term> extractTerminologyList(bool includeLinguisticFeatures, int termSpecificity, long limitNrOfTerms, int docCounter, string language);
		vector<Term> sortTerminologyByImportance(vector<Term> termList);
		vector<Term> mergesortTerminologyByImportance(vector<Term> termList, int start, int end);
		void mergeTerminologyByImportance(vector<Term>& mergedArr, vector<Term> leftTerms, vector<Term> rightTerms);
		Types getTypes();
		vector<string> getPOSTypes();
		void setTypes(Types ts);
		void setPOSTypes(vector<string> supportedPosTypes);
		void setPosTagger(postagger pt,string language);
		Validator getValidator();

		int setPosPatterns(string language);
		int setTermBlackLists(string language);
		int setNEBlackLists(string language);
		int setContextWeights(string language);
		int setWikiList (string language);
		int setBackgroundData(string language);
		vector<Term> extractTerminologyList(const int TermMaxLength, const int limitNrOfTerms);
		vector<Term> extractTerminologyList(const int TermMaxLength, int termSpecificity, long limitNrOfTerms, int docCounter, string language);
		vector<Term> extractTerminologyListFast(const int TermMaxLength, int termSpecificity, long limitNrOfTerms, int docCounter, string language);

		pair<int, int> getVersions();
};

#endif
