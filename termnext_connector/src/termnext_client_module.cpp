/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <iostream>
#include <string>
#include <map>
#include <boost/python.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include "termnext_client_module.h"
#include "termnext_system_module.h"
#include "GlobalMethods.h"
#include "termnext.h"
#include <json-c/json.h>

using namespace std;
using namespace boost::python;

Termnext_Client::Termnext_Client() {
    totalNrOfDocs=0;
    termnext = new Termnext();
}

void Termnext_Client::init_nemodule(const char* ner_path, const char* ner_model_path) {
    neTagger = termnext->getNETagger(ner_path, ner_model_path, termnext->getTypes());
}

void Termnext_Client::set_system_ref(Termnext_System& termnext_System, string language) {
    set_pos_model(termnext_System,language);
    set_supported_postags(termnext_System,language);
    set_supported_netypes(termnext_System,language);
    set_supported_pos_patterns(termnext_System,language);
    set_term_blacklists(termnext_System,language);
    set_ncweights(termnext_System,language);
    set_background_data(termnext_System,language);
    set_wikilists(termnext_System,language);
}

void Termnext_Client::set_pos_model(Termnext_System& termnext_System, string lang) {
    string language = "en";
    termnext->setPosTagger(termnext_System.sysLoader.ptagger, language);
}

void Termnext_Client::set_supported_postags(Termnext_System& termnext_System, string lang) {
    string language = "en";
    if((language=="en")||(language=="EN")) {
        supportedPosTypes = termnext_System.sysLoader.supportedPosTypes;
        termnext->setPOSTypes(supportedPosTypes);
    }
}

void Termnext_Client::set_supported_netypes(Termnext_System& termnext_System, string language) {
    if((language=="en")||(language=="EN")) {
        types = termnext_System.sysLoader.types;
        termnext->setTypes(types);
        Type t = types.getTypeByName("ORG");
    }
}
	
void Termnext_Client::set_supported_pos_patterns(Termnext_System& termnext_System, string language) {
    termnext->setPosPatterns(language);
}
	
void Termnext_Client::set_term_blacklists(Termnext_System& termnext_System, string language) {
    termnext->setTermBlackLists(language);
}
	
void Termnext_Client::set_ncweights(Termnext_System& termnext_System, string language) {
    termnext->setContextWeights(language);
}

void Termnext_Client::set_background_data (Termnext_System& termnext_System, string language) {
    termnext->setBackgroundData(language);
}
	
void Termnext_Client::set_wikilists(Termnext_System& termnext_System, string language) {
    termnext->setWikiList(language);
}
	
void Termnext_Client::push_sod(int nGrammCode) {
    nGrammCode = 5;  // set max ngram

    if(termnext->getValidator().isNGrammValueValid(nGrammCode)){
        cerr << "N-Gram value is valid .... " << endl;
    } else {
        cerr << "N-Gram is invalid. It should be an integer between 1 and " << MAX_NGRAM << endl;
    }

    termnext->pushSOD();
    termnext->configureNGramm(getNGramm((int)nGrammCode));
}

void Termnext_Client::push_data(string data, int docId, string language) {
    std::stringstream stream_;
    stream_ << data.c_str();
    cerr << "Language is "<< language << endl;
    termnext->pushData(stream_, docId, language);
}

void Termnext_Client::push_eod(int totalNrOfDocs) {
    this->totalNrOfDocs = totalNrOfDocs;
}

string Termnext_Client::escapeText(string text) {
    stringstream new_text;

    for(int i=0; i<text.size(); i++) {
        char c = text[i];
        switch (c) {
            case '\\':  new_text << '\\';
                        break;
            case '\"':  new_text << '\\';
                        break;
        }
    
        new_text << text[i];
    }

    return new_text.str();
}
	
string Termnext_Client::terminologylist(int includeLinguisticFeatures, int termSpecificity, int limitedNrOfElems, string language) {
    cerr << "Validating input parameter...." << endl;
    if(termnext->getValidator().isTermSpecificityValid(termSpecificity)) {
        cerr << "Term specificity is valid .... " << endl;
    } else {
        cerr << "Term specificity is invalid value. It should be an integer between 1 and 10 " << endl;
    }

    if(termnext->getValidator().isLimitedNrOfElementsValid(limitedNrOfElems)) {
        cerr << "Limited number of elements is valid .... " << endl;
    } else {
        cerr << "Limited number of elements is invalid. It should be an integer bigger than 0" << endl;
    }

    cerr << "IncludeLinguisticFeatures? " << includeLinguisticFeatures  << endl;

    vector<Term> termList = termnext->extractTerminologyListFast(5, (int)termSpecificity, (int)limitedNrOfElems, totalNrOfDocs, language);
		
    cerr << "Python-C"<< endl;
		
    // Transform results in jason format to the front
    vector<Term>::iterator termListIter = termList.begin();
    setlocale(LC_NUMERIC, "C");

    //Creating a json object
    json_object * jobj = json_object_new_object();

    //Creating a json array
    json_object *jarray = json_object_new_array();
		
    if (termList.size()==0) {
        json_object *jalgoVersion = json_object_new_string("");
        json_object *jdatabaseVersion = json_object_new_string("");
        json_object_object_add(jobj,"algov",jalgoVersion);
        json_object_object_add(jobj,"dsv",jdatabaseVersion);
        json_object_object_add(jobj,"terminology_list",jarray);
        return json_object_to_json_string(jobj);		  
    }
		
    json_object *jalgoVersion = json_object_new_int(termListIter->algorithmVersion);
    json_object *jdatabaseVersion = json_object_new_int(termListIter->databaseVersion);
    json_object_object_add(jobj,"algov",jalgoVersion);
    json_object_object_add(jobj,"dsv",jdatabaseVersion);
		
    // For Debuging: 
    // output the extracted terms
    // ofstream outfile;
    // outfile.open("outTerms.txt",std::fstream::app);

    while(termListIter != termList.end()) {
        // For Debuging: 
        // output the term scores
        // outfile << termListIter->value << "\t;\t" << termListIter->importanceScore;
        // map< int, std::list<pair<int,int> > > tmpdocs = termListIter->docs;
        // int occur=0;
        // for(map< int, std::list<pair<int,int> > > ::iterator iter = tmpdocs.begin(); iter != tmpdocs.end(); iter++) {
        // occur+=iter->second.size();		  
        // }
        // outfile << "\t;\t" << occur << "\t;\t" <<termListIter->getInfo()<< endl;

        json_object * jitem = json_object_new_object();
        json_object *jvalue = json_object_new_string(escapeText(termListIter->value).c_str());
        json_object *jscore = json_object_new_double(termListIter->importanceScore);
        json_object_object_add(jitem,"name",jvalue);
        json_object_object_add(jitem,"importance_score",jscore);

        json_object *jdocarray = json_object_new_array();
        map< int, std::list<pair<int,int> > > docs = termListIter->docs;
		 
        for(map< int, std::list<pair<int,int> > >::iterator docsiter = docs.begin(); docsiter != docs.end(); docsiter++){
            json_object * jdoc = json_object_new_object();
            json_object *jdocid = json_object_new_int(docsiter->first);
            json_object_object_add(jdoc,"id",jdocid);		    
		    
            std::list<pair<int,int> > positions = docsiter->second;
            json_object *jposarray = json_object_new_array();

            for(std::list<pair<int,int> >::iterator positionsiter = positions.begin(); positionsiter != positions.end(); positionsiter++){
                stringstream begin, end;
                string position;
                begin << positionsiter->first;
                position = begin.str()+":";
                end << positionsiter->second;
                position = position+end.str();		      
                json_object * jpos = json_object_new_string(position.c_str());

                json_object_array_add(jposarray,jpos);		      
            }		    
		    
            json_object_object_add(jdoc,"positions",jposarray);
            json_object_array_add(jdocarray, jdoc);
        }
		  
        json_object_object_add(jitem,"docs",jdocarray);
        json_object_array_add(jarray, jitem);
		   
        termListIter++;
    }
		
    json_object_object_add(jobj,"terminology_list",jarray);		
    return json_object_to_json_string(jobj);
}

string Termnext_Client::ne_receiventities4datachunk(string datachunk) {
    if(neTagger == NULL){
        return "Named-Entity system is not initialized";
    }

    vector<Phrase> phrases;
    neTagger->extractNamedEntities(phrases, datachunk.c_str(), termnext->getTypes());

    setlocale(LC_NUMERIC, "C");

    json_object * jobj = json_object_new_object();
 
    json_object *jarray = json_object_new_array();
 		
    for (int i=0; i<phrases.size(); i++) {
        Phrase phrase = phrases[i];
 
        json_object *jphraseStr = json_object_new_string(phrase.value.c_str());
        json_object *jphrasetype = json_object_new_int(phrase.t.id);
 			
        stringstream begin, end;
        string position;
        begin << phrase.cursorBegPos;
        position = begin.str()+":";
        end << phrase.cursorEndPos;
        position = position+end.str();
 
        json_object *jposition = json_object_new_string(position.c_str());
 			
        json_object * jNE = json_object_new_object();
        json_object_object_add(jNE,"name", jphraseStr);
        json_object_object_add(jNE,"type_id", jphrasetype);
        json_object_object_add(jNE,"cursor_pos", jposition);
 			
        json_object_array_add(jarray,jNE);
    }

    json_object_object_add(jobj,"tagged_entities", jarray);
    cerr << "NE-C Done" << endl;
 
    string returnStr = json_object_to_json_string(jobj);

    json_object_put(jarray);
    json_object_put(jobj);
		
    return returnStr;
}

string Termnext_Client::get_versions() {
    pair<int, int> v = termnext->getVersions(); 
    cerr << "Current Algorithm Version: " << v.first << " Database Version: " << v.second << endl;
	  
    stringstream versionbuffer;
    versionbuffer << " { \"algov\":" << v.first << " , \"dsv\":" << v.second << " }";  
    return versionbuffer.str();
}


/**
 * \brief Export termnext_client to Python
 *
 * This Boost.Python macro exports the class Termnext_Client to to python within the module termnext_client.
 */
BOOST_PYTHON_MODULE(termnext_client_module)
{
	class_<Termnext_Client>("Termnext_Client")
	.def("init_nemodule",               &Termnext_Client::init_nemodule)
	.def("set_system_ref",              &Termnext_Client::set_system_ref)
	.def("set_supported_postags",       &Termnext_Client::set_supported_postags)
	.def("set_supported_netypes",       &Termnext_Client::set_supported_netypes)
	.def("set_pos_model",               &Termnext_Client::set_pos_model)
	.def("set_pos_patterns",            &Termnext_Client::set_supported_pos_patterns)
	.def("set_term_blacklists",         &Termnext_Client::set_term_blacklists)
	.def("set_ncweights",               &Termnext_Client::set_ncweights)
	.def("set_background_data",         &Termnext_Client::set_background_data)
	.def("set_wikilists",               &Termnext_Client::set_wikilists)
	.def("push_sod",                    &Termnext_Client::push_sod)
	.def("push_data",                   &Termnext_Client::push_data)
	.def("push_eod",                    &Termnext_Client::push_eod)
	.def("terminologylist",             &Termnext_Client::terminologylist)
 	.def("ne_receiventities4datachunk", &Termnext_Client::ne_receiventities4datachunk)
	.def("get_versions",                &Termnext_Client::get_versions)
	.def(map_indexing_suite<std::map<int,int> >());
	
}

