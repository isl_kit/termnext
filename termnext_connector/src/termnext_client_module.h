/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef INTERPRETER_SUPPORT_TERMNEXT_CONNECTOR_TERMNEXT_CLIENT_MODULE_H_
#define	INTERPRETER_SUPPORT_TERMNEXT_CONNECTOR_TERMNEXT_CLIENT_MODULE_H_

#include <string>
#include "termnext.h"
#include "termnext_system_module.h"

using namespace std;
using namespace boost::python;

struct Termnext_Client {
	vector<string> supportedPosTypes;
	Types          types;
	Termnext*      termnext;
	NETagger*      neTagger;
	int            totalNrOfDocs;

	Termnext_Client();

	void init_nemodule(const char* ner_path, const char* ner_model_path);

	void set_system_ref(Termnext_System& eppsystem, string language);
	void set_pos_model(Termnext_System& eppsystem, string lang);
	void set_supported_postags(Termnext_System& eppsystem, string lang);
	void set_supported_netypes(Termnext_System& eppsystem, string language);
	void set_supported_pos_patterns(Termnext_System& eppsystem, string language);
	void set_term_blacklists(Termnext_System& eppsystem, string language);
	void set_ncweights(Termnext_System& eppsystem, string language);
	void set_background_data (Termnext_System& eppsystem, string language);
	void set_wikilists(Termnext_System& eppsystem, string language);
	
	void push_sod(int nGrammCode);
	void push_data(string data, int docId, string language);
	void push_eod(int totalNrOfDocs);

	string escapeText(string text);
	string terminologylist(int includeLinguisticFeatures, int termSpecificity, int limitedNrOfElems, string language);
	string ne_receiventities4datachunk(string datachunk);
  	string get_versions();
};

#endif	/* INTERPRETER_SUPPORT_TERMNEXT_CONNECTOR_TERMNEXT_CLIENT_MODULE_H_ */