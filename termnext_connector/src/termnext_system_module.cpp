/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


/* Total number of C API pointers */
#define PyISSYSTEM_API_pointers 1
#define PySpam_System_NUM 0

#include <stdio.h>
#include <iostream>
#include <string.h>
#include <boost/python.hpp>
#include "termnext_system_module.h"
#include "SystemLoader.h"
#include "lapos-0.1.2/postagger.h"


using namespace boost::python;
using namespace std;


/**
 * \brief This class represents a complete system for terminology and named entity extraction
 *
 * This class represents a complete system for terminology and named entity extraction.
 */
void Termnext_System::load_config(const char* config_path) {
    cout << "Loading Config..." << endl;
    sysLoader.init(config_path);
}

void Termnext_System::load_posmodel(const char* model_path) {
    cout << "Loading Pos model" << endl;
    sysLoader.initpostagger(model_path);
}

string Termnext_System::namedentity_types() {
    vector<Type> namedentityTypes = sysLoader.types.list();

    // Build named-entities json object
    stringstream values;
    values << "{\"all_named_entities\" :[";
    for(unsigned int i=0; i<namedentityTypes.size(); i++) {
        Type t = namedentityTypes[i];
        values << "{";
        values << "\"id\": \"";
        values << t.id;
        values << "\", \"name\": \"";
        values << t.name;
        values << "\" , \"label\": \"";
        values <<  t.label;
        values << "\", \"color\": \"";
        values << t.color;
        values << "\"}";

        if(i < (namedentityTypes.size() -1) ) {
            values << ",";
        }
    }

    values << "]}";
    cout << values.str() << endl;

    return values.str().c_str();
}


/**
 * \brief Export Termnext_System to Python
 *
 * This Boost.Python macro exports the class @{link Termnext_System Termnext_System} to python as module termnext_system.
 */
BOOST_PYTHON_MODULE(termnext_system_module) {
	class_<Termnext_System>("Termnext_System")
    		.def("load_config", &Termnext_System::load_config)
    		.def("namedentity_types", &Termnext_System::namedentity_types)
    		.def("load_posmodel", &Termnext_System::load_posmodel);
}

