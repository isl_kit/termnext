/* ========================================================================
    TermNEXt
               ------------------------------------------------------------
    Authors :  Narine Kokhlikyan, Yuqi Zhang, Sebastian Stüker
  
   ========================================================================
    
    Copyright 2014 

        Karlsruher Institut für Technologie
        Institute für Anthropomatik und Robotik
        Bereich Waibel
        Adenauerring 2
        76131 Karlsruhe
        Germany
   ========================================================================

    This file is part of TermNEXt.

    TermNEXt is free software: you can redistribute it and/or modify
    it under the terms of the Lesser GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TermNEXt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Lesser GNU General Public License for more details.

    You should have received a copy of the Lesser GNU General Public License
    along with TermNEXt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef INTERPRETER_SUPPORT_TERMNEXT_CONNECTOR_TERMNEXTSYSTEM_MODULE_H_
#define	INTERPRETER_SUPPORT_TERMNEXT_CONNECTOR_TERMNEXTSYSTEM_MODULE_H_

#include <string.h>
#include "SystemLoader.h"

struct Termnext_System {
		SystemLoader sysLoader;
		void load_config(const char* config_path);
		void load_posmodel(const char* model_path);
		string namedentity_types();
};

#endif	/* INTERPRETER_SUPPORT_TERMNEXT_CONNECTOR_TERMNEXTSYSTEM_MODULE_H_ */

